#include "sched.h"

#include "console.h"
#include "errno.h"
#include "file.h"
#include "gdt.h"
#include "lib.h"
#include "mm.h"
#include "tty.h"

#define START_QUANTUM 3

extern uint32_t KERNEL_START;

struct task *current_task;

static struct kthread_list {
	struct task *node;
	struct kthread_list *next;
} * kthread_list;

static uint32_t pid = -1;

#define USTACK_TOP ((uint32_t)&KERNEL_START)
#define USTACK_SIZE (PAGE_SIZE * 10)
#define KSTACK_SIZE 1024
#define GET_STACK_TOP_VA(kthread)                          \
	((uint32_t)((kthread->type == TASK_KTHREAD) ?      \
			    kthread->stack + KSTACK_SIZE : \
			    pa2va(kthread->stack) + USTACK_SIZE))

static void initialize_kstack(struct task *kthread, int argv_envp_size)
{
	uint32_t stack_top = kthread->regs.esp;
	uint32_t stack_addr = stack_top;
	if (kthread->type == TASK_USER) {
		stack_addr -= 4;
		*(uint32_t *)stack_addr = kthread->regs.ss;
		stack_addr -= 4;
		/* leave two slots for argv & envp */
		*(uint32_t *)stack_addr = USTACK_TOP - argv_envp_size - 12;
	}

	stack_addr -= 4;
	*(uint32_t *)stack_addr = kthread->regs.eflags;
	stack_addr -= 4;
	*(uint32_t *)stack_addr = kthread->regs.cs;
	stack_addr -= 4;
	*(uint32_t *)stack_addr = kthread->regs.eip;

	/* (fake) trapno & errcode */
	stack_addr -= 8;

	/* Push 4 segment selectors */
	stack_addr -= 4;
	*(uint32_t *)stack_addr = kthread->regs.ds;
	stack_addr -= 4;
	*(uint32_t *)stack_addr = kthread->regs.es;
	stack_addr -= 4;
	*(uint32_t *)stack_addr = kthread->regs.fs;
	stack_addr -= 4;
	*(uint32_t *)stack_addr = kthread->regs.gs;

	/* Perform pusha on allocated memory */
	stack_addr -= 4;
	*(uint32_t *)stack_addr = kthread->regs.eax;
	stack_addr -= 4;
	*(uint32_t *)stack_addr = kthread->regs.ecx;
	stack_addr -= 4;
	*(uint32_t *)stack_addr = kthread->regs.edx;
	stack_addr -= 4;
	*(uint32_t *)stack_addr = kthread->regs.ebx;
	/* oesp - skip */
	stack_addr -= 4;
	stack_addr -= 4;
	*(uint32_t *)stack_addr = kthread->regs.ebp;
	stack_addr -= 4;
	*(uint32_t *)stack_addr = kthread->regs.esi;
	stack_addr -= 4;
	*(uint32_t *)stack_addr = kthread->regs.edi;

	kthread->regs.esp -= (uint32_t)(stack_top - stack_addr);
}

/* FIXME: Clumsy interface. If parent is 0, use proc to set eip and default-initialize the rest.
 * If parent is present, we're doing a fork, and tf is valid also. Deep copy the appropriate data.
 */
struct task *create_task(void (*proc)(void), char *argv[], char *envp[],
			 enum task_type type, struct task *parent,
			 struct trapframe *tf)
{
	struct task *kthread = kmalloc(sizeof(struct task));
	memset(kthread, 0, sizeof(struct task));

	kthread->type = type;
	kthread->state = TASK_WAITING;
	kthread->quantum = START_QUANTUM;
	kthread->parent = parent;

	if (kthread->type == TASK_USER) {
		void *stack_page = get_page();
		for (int i = 1; i < 10; i++)
			if (get_page() != stack_page + i * PAGE_SIZE)
				panic("Stack pages are non-consecutive.");
		struct mapping *stack_mapping =
			kzmalloc(sizeof(struct mapping));
		stack_mapping->region.va = USTACK_TOP - USTACK_SIZE;
		stack_mapping->region.pa = stack_page;
		stack_mapping->region.length = USTACK_SIZE;
		stack_mapping->type = MAPPING_STACK;
		add_mapping(kthread, stack_mapping);
		kthread->stack = stack_page;
	} else {
		kthread->stack = kmalloc(KSTACK_SIZE);
	}

	kthread->kstack = kmalloc(KSTACK_SIZE);

	if (parent && tf) {
		memcpy(pa2va(kthread->stack), pa2va(parent->stack),
		       USTACK_SIZE);
		memcpy(kthread->kstack, parent->kstack, KSTACK_SIZE);
		kthread->regs.esp = kthread->kstack + KSTACK_SIZE -
				    (parent->kstack + KSTACK_SIZE - (char *)tf);
		/* fork() return value */
		((struct trapframe *)(kthread->regs.esp))->eax = 0;
	} else {
		kthread->regs.eip = (uint32_t)proc;
		kthread->regs.eflags = (uint32_t)0x3202; /* setting IF */
		kthread->regs.cs = kthread->type != TASK_KTHREAD ?
					   (SEG_UCODE << 3) | 0x3 :
					   SEG_KCODE << 3;
		kthread->regs.ds = kthread->regs.es = kthread->regs.fs =
			kthread->regs.gs = kthread->regs.ss =
				kthread->type != TASK_KTHREAD ?
					(SEG_UDATA << 3) | 0x3 :
					SEG_KDATA << 3;
		if (kthread->type == TASK_USER) {
			if (argv || envp) {
				/* Determine the number of bytes set aside for the strings in argv & envp. */
				int total_length = 0;
				int argc = 0, envc = 0;
				for (const char **str = argv; *str; str++) {
					argc++;
					total_length += strlen(*str) + 1;
				}
				for (const char **str = envp; *str; str++) {
					envc++;
					total_length += strlen(*str) + 1;
				}

				/* align to 32 bit */
				total_length =
					round_up_multiple(total_length, 4);

				/* Copy the strings while filling argv & envp further down on the stack. */
				int current_length = 0;

				int i = 0;
				*(uint32_t *)(pa2va(kthread->stack) +
					      USTACK_SIZE - total_length -
					      (envc + 1) * 4 - 4) = 0;
				for (const char **str = argv; *str; str++) {
					memcpy(pa2va(kthread->stack) +
						       USTACK_SIZE -
						       total_length +
						       current_length,
					       *str, strlen(*str) + 1);
					*(uint32_t *)(pa2va(kthread->stack) +
						      USTACK_SIZE -
						      total_length -
						      (envc + 1) * 4 -
						      (argc + 1) * 4 +
						      (i++ * 4)) =
						USTACK_TOP - total_length +
						current_length;
					current_length += strlen(*str) + 1;
				}

				i = 0;
				*(uint32_t *)(pa2va(kthread->stack) +
					      USTACK_SIZE - total_length - 4) =
					0;
				for (const char **str = envp; *str; str++) {
					memcpy(pa2va(kthread->stack) +
						       USTACK_SIZE -
						       total_length +
						       current_length,
					       *str, strlen(*str) + 1);
					*(uint32_t *)(pa2va(kthread->stack) +
						      USTACK_SIZE -
						      total_length -
						      (envc + 1) * 4 +
						      (i++ * 4)) =
						USTACK_TOP - total_length +
						current_length;
					current_length += strlen(*str) + 1;
				}

				/* Put pointers to the created arrays on the top of the stack. */
				*(uint32_t *)(pa2va(kthread->stack) +
					      USTACK_SIZE - total_length -
					      (envc + 1) * 4 - (argc + 1) * 4 -
					      4) = USTACK_TOP - total_length -
						   (envc + 1) * 4;
				*(uint32_t *)(pa2va(kthread->stack) +
					      USTACK_SIZE - total_length -
					      (envc + 1) * 4 - (argc + 1) * 4 -
					      8) = USTACK_TOP - total_length -
						   (envc + 1) * 4 -
						   (argc + 1) * 4;
				kthread->regs.esp =
					(uint32_t)(pa2va(kthread->stack) +
						   USTACK_SIZE - total_length -
						   (envc + 1) * 4 -
						   (argc + 1) * 4 - 12);
				initialize_kstack(
					kthread, total_length + (envc + 1) * 4 +
							 (argc + 1) * 4);
			} else {
				*(uint32_t *)(pa2va(kthread->stack) +
					      USTACK_SIZE - 4) = 0;
				*(uint32_t *)(pa2va(kthread->stack) +
					      USTACK_SIZE - 8) = 0;
				kthread->regs.esp =
					(uint32_t)(pa2va(kthread->stack) +
						   USTACK_SIZE - 12);
				initialize_kstack(kthread, 0);
			}
		} else {
			kthread->regs.esp =
				(uint32_t)kthread->stack +
				KSTACK_SIZE; /* growing downward, empty */
			initialize_kstack(kthread, 0);
		}
	}

	struct kthread_list *end;
	if (!kthread_list) {
		end = kthread_list = kmalloc(sizeof(struct kthread_list));
		memset(kthread_list, 0, sizeof(struct kthread_list));
	} else {
		end = kthread_list;
		while (end->next)
			end = end->next;
		end->next = kmalloc(sizeof(struct kthread_list));
		memset(end->next, 0, sizeof(struct kthread_list));
		end = end->next;
	}

	end->node = kthread;

	if (type == TASK_KTHREAD) {
		kthread->page_directory = va2pa(page_directory);
	} else {
		void *new_pgdir = get_page();
		memcpy(pa2va(new_pgdir), page_directory, 4096);
		kthread->page_directory = new_pgdir;

		/* Map user mode stack so that return from the first switch_to() goes smoothly. */
		void *stack_page_table = get_page();
		*((uint32_t *)pa2va(new_pgdir) + 767) =
			(uint32_t)stack_page_table | 0x7;
		for (int i = 0; i < 10; i++)
			*((uint32_t *)pa2va(stack_page_table) + 1023 - i) =
				(uint32_t)(kthread->stack + (9 - i) * 4096) |
				0x7;
	}

	if (parent && tf /* in fork(), not exec() */) {
		/* Copy all physical pages (except the stack), fill the mapping linked list and await page faults. */
		/* FIXME: extract static copy_mapping() */
		struct mapping *parent_mapping = parent->mapping;
		struct mapping *child_mapping =
			kzmalloc(sizeof(struct mapping));
		while (parent_mapping->type == MAPPING_STACK) {
			parent_mapping = parent_mapping->next;
		}

		child_mapping->region.va = parent_mapping->region.va;
		child_mapping->region.pa = 0;
		while (parent_mapping != 0) {
			if (child_mapping->region.pa != 0) {
				add_mapping(kthread, child_mapping);
				child_mapping =
					kzmalloc(sizeof(struct mapping));
				child_mapping->region.pa = 0;
				child_mapping->region.va =
					parent_mapping->region.va;
			}

			int offset = 0;
			while (offset < parent_mapping->region.length) {
				uint32_t page = get_page();
				uint32_t chunk_length = min(
					4096,
					parent_mapping->region.length - offset);
				memcpy(pa2va(page),
				       pa2va(parent_mapping->region.pa +
					     offset),
				       chunk_length);

				if (child_mapping->region.pa == 0) {
					child_mapping->region.pa = page;
					child_mapping->region.length =
						chunk_length;
				} else if (child_mapping->region.pa +
						   child_mapping->region.length ==
					   page) {
					child_mapping->region.length +=
						chunk_length;
				} else {
					add_mapping(kthread, child_mapping);
					child_mapping = kzmalloc(
						sizeof(struct mapping));
					child_mapping->region.pa = page;
					child_mapping->region.va =
						parent_mapping->region.va +
						offset;

					//child_mapping->region.va =
					//	offset < parent_mapping->region.length
					//	? parent_mapping->region.va + offset
					//	: (parent_mapping->next ? parent_mapping->next->region.va : 0);
					child_mapping->region.length =
						chunk_length;
				}

				offset += chunk_length;
			}

			parent_mapping = parent_mapping->next;
		}

		if (child_mapping->region.pa != 0)
			add_mapping(kthread, child_mapping);
	}

	kthread->exit_code = 0;
	kthread->pid = (!parent || tf) ? ++pid : pid;
	kthread->cwd = parent ? parent->cwd : path_lookup("/", 0);

	if (parent) {
		kthread->ppid = parent->pid;
		for (int i = 0; i < MAX_FILES; i++) {
			if (parent->files[i]) {
				kthread->files[i] =
					kmalloc(sizeof(struct file));
				memcpy(kthread->files[i], parent->files[i],
				       sizeof(struct file));
			}
		}
	}

	kthread->child_task_list = 0;
	if (parent) {
		if (!parent->child_task_list) {
			parent->child_task_list =
				kmalloc(sizeof(struct child_task_list));
			parent->child_task_list->pid = kthread->pid;
			parent->child_task_list->next = 0;
		} else {
			struct child_task_list *child_task_list =
				parent->child_task_list;
			while (child_task_list->next != 0)
				child_task_list = child_task_list->next;
			child_task_list->next =
				kmalloc(sizeof(struct child_task_list));
			child_task_list->next->pid = kthread->pid;
			child_task_list->next->next = 0;
		}
	}

	/* Copy files or preopen std streams. */
	if (!parent) {
		for (int i = 0; i < 3; i++) {
			struct file *tty_file = kmalloc(sizeof(struct file));
			tty_file->inode = tty_inode;
			kthread->files[i] = tty_file;
		}
	}

	kthread->tty = tty_device;

	return kthread;
}

static struct task *get_task_by_pid(uint32_t pid)
{
	if (!kthread_list)
		return 0;

	struct kthread_list *kthread = kthread_list;
	while (kthread != 0) {
		if (kthread->node->pid == pid)
			return kthread->node;

		kthread = kthread->next;
	}

	return 0;
}

#define threadproc(num)                                           \
	void do_nothing##num(void)                                \
	{                                                         \
		while (1) {                                       \
			/* term_print_ok("In task " #num "."); */ \
		}                                                 \
	}
threadproc(0)

	int sched_init(void)
{
#define createkthread(num) \
	create_task(do_nothing##num, 0, 0, TASK_KTHREAD, 0, 0);
	createkthread(
		0) /* mandatory idle kthread. FIXME: make lowest-priority */

		term_print_ok("Scheduler initialized.");
	return 0;
}

int switch_to(struct task *from, struct task *to, struct trapframe *tf)
{
	/* Switch page directory. Safe because kernel mapping remains the same. */
	asm volatile("mov %0, %%eax" : : "r"(to->page_directory));
	asm volatile("mov %eax, %cr3");

	if (from)
		from->regs.esp = tf;

	tf->oesp = to->regs.esp;
	if (to->type != TASK_KTHREAD)
		gdt_set_esp0(to->kstack + KSTACK_SIZE);

	asm volatile("sti");

	return 0;
}

int destroy_task(struct task *kthread)
{
	struct kthread_list *curr = kthread_list, *prev = 0;
	while (curr->node != kthread) {
		prev = curr;
		curr = curr->next;
	}

	if (prev) {
		prev->next = curr->next;
	} else {
		kthread_list = curr->next;
	}

	struct mapping *curr_mapping = curr->node->mapping;
	while (curr_mapping != 0) {
		struct mapping *next_mapping = curr_mapping->next;
		kfree(curr_mapping);
		curr_mapping = next_mapping;
	}

	if (curr->node->parent) {
		struct child_task_list *child_task = curr->node->parent
							     ->child_task_list,
				       *prev_child_task = 0;
		while (child_task->pid != curr->node->pid) {
			prev_child_task = child_task;
			child_task = child_task->next;
		}

		if (prev_child_task) {
			prev_child_task->next = child_task->next;
		} else {
			curr->node->parent->child_task_list = child_task->next;
		}
	}

	/* FIXME: deliberate memory leak until I fix the consecutiveness requirement on curr->node->stack */
	//	if (curr->node->type != TASK_KTHREAD) {
	//		put_page(curr->node->page_directory);
	//		put_page(curr->node->stack);
	//	}

	kfree(curr->node->kstack);
	kfree(curr->node);
	kfree(curr);

	return 0;
}

static void schedule_new(struct trapframe *tf)
{
	struct task *prev = current_task;
	struct kthread_list *kthread;

	if (prev && prev->state == TASK_RUNNING)
		prev->state = TASK_EXPIRED;
	for (kthread = kthread_list; kthread != 0; kthread = kthread->next) {
		if (kthread->node->state == TASK_DEAD) {
			if (kthread->node == current_task)
				prev = 0;
			destroy_task(kthread->node);
		} else if (kthread->node->state == TASK_WAITING) {
			break;
		}
	}

	if (kthread) {
		current_task = kthread->node;
	} else {
		for (struct kthread_list *kthread = kthread_list; kthread != 0;
		     kthread = kthread->next) {
			if (kthread->node->state != TASK_EXITED) {
				kthread->node->state = TASK_WAITING;
				kthread->node->quantum = START_QUANTUM;
			}
		}
		current_task = kthread_list->node;
	}

	switch_to(prev, current_task, tf);
	current_task->state = TASK_RUNNING;
}

int sched_tick(struct trapframe *tf)
{
	int retval = 0;

	asm volatile("cli");

	/* No kthreads in system? */
	if (!kthread_list) {
		retval = 0;
		goto out;
	}

	if (!current_task || !current_task->quantum--) {
		schedule_new(tf);
		retval = 1;
	}

out:
	asm volatile("sti");
	return retval;
}

void add_mapping(struct task *task, struct mapping *mapping)
{
	if (!task->mapping) {
		task->mapping = mapping;
		return;
	}

	struct mapping *task_mapping = task->mapping;
	while (task_mapping->next != 0)
		task_mapping = task_mapping->next;
	task_mapping->next = mapping;
}

int sys_exit(struct trapframe *tf)
{
	asm volatile("cli");
	current_task->state = TASK_EXITED;
	current_task->exit_code = tf->ebx;

	char msg[100];
	sprintf(msg, "process %d died with code %d", current_task->pid,
		current_task->exit_code);
	term_print_info(msg);

	schedule_new(tf);

	asm volatile("sti");
	return 0;
}

int sys_getpid(struct trapframe *tf)
{
	return current_task->pid;
}

int sys_fork(struct trapframe *tf)
{
	asm volatile("cli");
	struct task *task = create_task((void (*)(void))tf->eip, 0, 0,
					TASK_USER, current_task, tf);

	asm volatile("sti");
	return task->pid;
}

int sys_wait(struct trapframe *tf)
{
	asm volatile("sti");

	int *status = (int *)tf->ecx;
	int retval = -1;
	while (1) {
		asm volatile("cli");
		struct child_task_list *child_task =
			current_task->child_task_list;
		while (child_task != 0) {
			struct task *task = get_task_by_pid(child_task->pid);
			if (!task)
				panic("Can't find child task in kthread_list");

			if (task->state == TASK_EXITED &&
			    ((int)tf->ebx == -1 || (int)tf->ebx == task->pid)) {
				retval = task->pid;
				if (status)
					*status = task->exit_code;
				destroy_task(task);
				goto out;
			}
			child_task = child_task->next;
		}
		asm volatile("sti");
	}

out:
	asm volatile("sti");
	return retval;
}

int sys_getuid(struct trapframe *tf)
{
	return 0;
}

int sys_getgid(struct trapframe *tf)
{
	return 0;
}

int sys_geteuid(struct trapframe *tf)
{
	return 0;
}

int sys_getegid(struct trapframe *tf)
{
	return 0;
}

int sys_getppid(struct trapframe *tf)
{
	return 0;
}
