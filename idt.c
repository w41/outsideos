#include <stddef.h>
#include <stdint.h>

#include "console.h"
#include "gdt.h"
#include "idt.h"
#include "keyboard.h"
#include "lib.h"
#include "mm.h"
#include "ports.h"
#include "sched.h"

static struct idtdesc idts[256];
uint64_t jiffies;
extern uint32_t vectors[];
extern void kbint();
extern void pf();

extern void term_print(const char *str);
extern void term_putc(char c);

static inline void lidt(struct idtdesc *idts, int size)
{
	volatile uint16_t w[3];
	w[0] = size - 1;
	w[1] = (uintmax_t)idts;
	w[2] = ((uintmax_t)idts) >> 16;

	asm volatile("lidt (%0)" : : "r"(w));
}

static void remap_pic(void)
{
	outb(PIC1_COMMAND,
	     ICW1_INIT +
		     ICW1_ICW4); // starts the initialization sequence (in cascade mode)
	io_wait();
	outb(PIC2_COMMAND, ICW1_INIT + ICW1_ICW4);
	io_wait();
	outb(PIC1_DATA, PIC1_OFFSET); // ICW2: Master PIC vector offset
	io_wait();
	outb(PIC2_DATA, PIC2_OFFSET); // ICW2: Slave PIC vector offset
	io_wait();
	outb(PIC1_DATA,
	     4); // ICW3: tell Master PIC that there is a slave PIC at IRQ2 (0000 0100)
	io_wait();
	outb(PIC2_DATA,
	     2); // ICW3: tell Slave PIC its cascade identity (0000 0010)
	io_wait();

	outb(PIC1_DATA, ICW4_8086);
	io_wait();
	outb(PIC2_DATA, ICW4_8086);
	io_wait();
}

void trap(struct trapframe *tf)
{
	unsigned int t = 0;
	char c;
	char str[100];
	switch (tf->trapno) {
	case 13:
		term_print("General Protection Fault\n");
		break;
	case 14:
		handle_page_fault(tf->err);
		break;
	case 32:
		sched_tick(tf);
		jiffies++;
		break;
	case 33:
		keyboard_getc();
		break;
	case 39:
		/* spurious IRQ */
		outb(0x20, 0x0b);
		io_wait();
		if (!(inb(0x20) & 0x80))
			return;
		break;
	case 0x80:
		syscall(tf);
		break;
	default:
		sprintf(str, "Unhandled interrupt: 0x%x", tf->trapno);
		term_print_error(str);
		break;
	}

	if (tf->trapno <= 0xA7 && tf->trapno >= 0xA0) {
		outb(PIC1_COMMAND, 0x20);
		outb(PIC2_COMMAND, 0x20);
	} else if (tf->trapno <= 0x27 && tf->trapno >= 0x20) {
		outb(PIC1_COMMAND, 0x20);
	}
}

int idt_init()
{
	//	uint32_t apic_msr = 0x1B;
	//	uint32_t lo, hi;
	//	asm volatile ("rdmsr" : "=a"(lo), "=d"(hi) : "c"(apic_msr));
	//	lo &= ~(1 << 11);
	//	asm volatile ("wrmsr" : : "a"(lo), "d"(hi), "c"(apic_msr));

	//	io_wait();

	remap_pic();

	for (int i = 0; i < 128; i++) {
		idts[i] = DEFINT(vectors[i], SEG_KCODE << 3, 0);
	}

	for (int i = 128; i < 256; i++) {
		idts[i] = DEFINT(vectors[i], SEG_KCODE << 3, 3);
	}

	lidt(idts, sizeof(idts));

	term_print_ok("Interrupt handler setup complete.");

	return 0;
}

void unmask_pic()
{
	outb(PIC1_DATA, 0xFC); /* Unmask PIT & keyboard. */
	outb(PIC2_DATA, 0xFF);
}
