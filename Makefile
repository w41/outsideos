SHELL := /bin/bash
CC ?= i686-outsideos-gcc

SUBDIRS = dev fs

SRCFILES := $(shell find . -name "*.[cS]")
OBJFILES := gdt.o idt.o kernel.o lib.o vectors.o idt_handlers.o boot.o console.o keyboard.o mm.o panic.o timer.o sched.o exec.o syscall.o file.o tty.o

CFLAGS := -std=gnu99 -ffreestanding -O0 -g
ASFLAGS := -std=gnu99 -ffreestanding -g
LDFLAGS := -ffreestanding -nostdlib -g -T kernel.ld

LOOPDEVICE := $(shell sudo losetup -f)

all: kernel.elf

$(SUBDIRS):
	$(MAKE) -C $@ CFLAGS="$(CFLAGS)"

clean:
	-@$(RM) -rf $(wildcard $(OBJFILES) kernel.elf *.img bochslog.txt vectors.S img)
	$(foreach subdir, $(SUBDIRS), $(MAKE) -C $(subdir) clean &&) true

qemu: kernel.img
	qemu-system-i386 -monitor telnet:127.0.0.1:55555,server,nowait -drive file=kernel.img,format=raw -display curses -m 256M

qemu-gdb: kernel.img
	qemu-system-i386 -monitor telnet:127.0.0.1:55555,server,nowait -drive file=kernel.img,format=raw -display curses -m 256M -s -S

kernel.elf: $(OBJFILES) $(SUBDIRS)
	$(CC) $(OBJFILES) $(foreach subdir, $(SUBDIRS), $(subdir)/subdir.o) $(LDFLAGS) -o kernel.elf -lgcc

%.o: %.c Makefile
	$(CC) $(CFLAGS) -c $< -o $@

%.o: %.S Makefile
	$(CC) $(ASFLAGS) -c $< -o $@

vectors.S: vectors.pl
	perl vectors.pl > vectors.S

kernel.img: kernel.elf
	mkdir -p img/boot/grub
	cp kernel.elf img/boot
	cp grub.cfg img/boot/grub
	qemu-img create initrd.img 64M
	/sbin/mkfs.vfat -F 32 -n derp -s 2 initrd.img
	sudo losetup $(LOOPDEVICE) initrd.img
	mkdir mnt
	sudo mount -t vfat -o rw,umask=0 $(LOOPDEVICE) ./mnt
	mkdir -p ./mnt/out/in
	mkdir -p ./mnt/out/bigandverylongname123456.ext
	mkdir -p ./mnt/out2/in3
	mkdir ./mnt/bin
	mkdir ./mnt/out/dir{1..50}
	printf derp > ./mnt/out/in/testfile
	printf "#include<stdio.h>\nint main() { printf(\"hello world\"); }" > ./mnt/prog.c
	i686-outsideos-gcc -m32 -static -o ./mnt/bin/prog ./mnt/prog.c
	cp ./mnt/bin/prog prog
	cp ../outsideos-userspace/build/bash/bash ./mnt/bin/bash
	sudo losetup -d $(LOOPDEVICE)
	sudo umount mnt
	rm -rf mnt
	cp initrd.img img/boot
	grub-mkrescue -o kernel.img img

bochs: .bochsrc kernel.img
	bochs -q

FORCE:

.PHONY: $(SUBDIRS)
