#ifndef TTY_H
#define TTY_H

#include "file.h"

#define TTY_BUF_SIZE 32

struct tty_device {
	char buf[TTY_BUF_SIZE];
	int count;
	int pos;
};

extern struct inode *tty_inode;
extern struct tty_device *tty_device;

void tty_init();
int tty_read(struct tty_device *tty, void *buf, int count);
int tty_write(struct tty_device *tty, void *buf, int count);
void tty_putchar(struct tty_device *tty, int c);

#endif
