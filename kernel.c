// GCC provides these header files automatically
// They give us access to useful things like fixed-width types
#include <stddef.h>
#include <stdint.h>

#include "console.h"
#include "gdt.h"
#include "idt.h"
#include "keyboard.h"
#include "mm.h"
#include "multiboot.h"
#include "sched.h"
#include "timer.h"
#include "tty.h"

#include "fs/fat.h"

// First, let's do some basic checks to make sure we are using our x86-elf cross-compiler correctly
#if defined(__linux__)
#error "This code must be compiled with a cross-compiler"
#elif !defined(__i386__)
#error "This code must be compiled with an x86-elf compiler"
#endif

static void task_1()
{
	char *const argv[] = {"/bin/bash", 0};
	char *const envp[] = {0};
	asm volatile("cli");
	execve("/bin/bash", argv, envp);
	asm volatile("sti");
}

// This is our kernel's main function
void kernel_main(uint32_t mbmagic, multiboot_info_t *mbinfo)
{
	term_init();

	gdt_init();
	idt_init();

	mm_init(mbinfo);
	tty_init();

	sched_init();

	timer_init();
	keyboard_init();

	fat_init();

	create_task(task_1, (const char *[]){0}, (const char *[]){0}, TASK_USER,
		    0, 0);

	unmask_pic();

	// Display some messages
	term_print_ok("Initialization complete.");
	term_print("Welcome to the kernel!\n");

	asm volatile("sti");

	while (1) {
	}
}
