#ifndef ERRNO_H
#define ERRNO_H

/* POSIX common error identifiers. */
#define ENOENT 2
#define ENOEXEC 8
#define EBADF 9 /* Bad file descriptor */
#define ENOMEM 12
#define ENOTDIR 20
#define EINVAL 22
#define EMFILE 24 /* Too many open files for this task */
#define ENOTTY 25

#endif
