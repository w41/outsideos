#ifndef MM_H
#define MM_H

#include <stddef.h>
#include <stdint.h>

#include "idt.h"
#include "multiboot.h"

#define PAGE_SIZE 4096

extern uint32_t KERNEL_START;
#define va2pa(addr) ((uint32_t)(addr) - (uint32_t)&KERNEL_START)
#define pa2va(addr) ((uint32_t)(addr) + (uint32_t)&KERNEL_START)

int mm_init(multiboot_info_t *mbinfo);
void *get_page(void);
void put_page(void *);

void *kmalloc(size_t size);
void *kzmalloc(size_t size);
void kfree(void *addr);

void handle_page_fault(uint32_t err);

extern uint32_t page_directory[1024];

int sys_sbrk(struct trapframe *tf);

#endif
