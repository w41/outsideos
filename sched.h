#ifndef SCHED_H
#define SCHED_H

#define KSTACK_SIZE 0x400

#ifndef __ASSEMBLER__
#include "idt.h"

#include "fs/fat.h"

/* A consecutive physmem region mapped into a task's virtual space. */
/* FIXME: currently has to be aligned to a page boundary. */

struct mem_region {
	uint32_t va;
	uint32_t pa;
	int length;
};

struct mapping {
	struct mem_region region;
	enum mapping_type { MAPPING_UNKNOWN, MAPPING_STACK, MAPPING_HEAP } type;
	struct mapping *next;
};

struct child_task_list {
	uint32_t pid;
	struct child_task_list *next;
};

#define MAX_FILES 10

struct task {
	int pid;
	int ppid;

	struct trapframe regs;

	enum task_type { TASK_USER, TASK_KTHREAD } type;
	enum task_state {
		TASK_WAITING,
		TASK_RUNNING,
		TASK_EXPIRED,
		TASK_EXITED,
		TASK_DEAD
	} state;

	int quantum;
	struct task *parent;

	struct trapframe *tf;

	/* pa of stack for userspace tasks, va for kthreads. */
	char *stack;

	/* FIXME: To be reimplemented. */
	char *kstack;

	uint32_t *page_directory;
	struct mapping *mapping;
	uint32_t brk;

	int exit_code;

	struct file *files[MAX_FILES];

	struct child_task_list *child_task_list;

	struct tty_device *tty;

	struct dentry *cwd;
};

struct task *create_task(void (*proc)(void), char *argv[], char *envp[],
			 enum task_type type, struct task *parent,
			 struct trapframe *tf);
int destroy_task(struct task *task);
int switch_to(struct task *from, struct task *to, struct trapframe *tf);

int sched_init(void);
/* Return 1 if the scheduler initiated a context switch. */
int sched_tick(struct trapframe *tf);

/* Takes a preallocated linked list of mem regions and assumes ownership. */
void add_mapping(struct task *task, struct mapping *mapping);
void remove_mapping(struct task *task, void *va);

extern struct task *current_task;

int sys_exit(struct trapframe *tf);
int sys_getpid(struct trapframe *tf);
int sys_fork(struct trapframe *tf);
int sys_wait(struct trapframe *tf);
int sys_getuid(struct trapframe *tf);
int sys_getgid(struct trapframe *tf);
int sys_geteuid(struct trapframe *tf);
int sys_getegid(struct trapframe *tf);
int sys_getppid(struct trapframe *tf);

#endif
#endif
