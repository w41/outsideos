#ifndef LIB_H
#define LIB_H

#include <stddef.h>

#define min(x, y) (((x) < (y)) ? (x) : (y))

#define likely(x) __builtin_expect(x, 1)
#define unlikely(x) __builtin_expect(x, 0)

int sprintf(char *str, const char *format, ...);
char *strcpy(char *dest, const char *src);
char *strncpy(char *dest, const char *src, int n);
char *strchr(const char *s, int c);
char *strdup(const char *s);
int strlen(const char *str);

int strcmp(const char *s1, const char *s2);
int strncmp(const char *s1, const char *s2, int n);

unsigned int round_up_multiple(unsigned int num, unsigned int div);
unsigned int round_down_multiple(unsigned int num, unsigned int div);
void *memset(void *dest, int c, size_t n);
void *memcpy(void *dest, const void *src, size_t n);

void *kmalloc(size_t size);
void *kzmalloc(size_t size);
void kfree(void *ptr);

#endif
