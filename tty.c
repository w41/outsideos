#include "tty.h"

#include "console.h"
#include "errno.h"
#include "lib.h"

struct tty_device *tty_device;
struct inode *tty_inode;

void tty_init()
{
	tty_inode = kmalloc(sizeof(struct inode));
	memset(tty_inode, 0, sizeof(struct inode));
	tty_inode->mode = S_IFCHR;

	tty_device = kmalloc(sizeof(struct tty_device));
	memset(tty_device, 0, sizeof(struct tty_device));
}

int tty_read(struct tty_device *tty, void *buf, int count)
{
	if (count == 0)
		return 0;

	count = min(count, tty->count);
	tty->count -= count;
	int tail = min(count, TTY_BUF_SIZE - tty->pos);
	if (count > 0) {
		memcpy(buf, tty->buf + tty->pos, tail);
		count -= tail;
		if (count > 0) {
			memcpy(buf + tail, tty->buf, count);
		}
		tty->pos = (tty->pos + count + tail) % TTY_BUF_SIZE;
		return count + tail;
	} else {
		/* bash busy loop */
		((char *)buf)[0] = 0;
		return 1;
	}
}

int tty_write(struct tty_device *tty, void *buf, int count)
{
	if (count >= 100)
		return -EINVAL;
	char msg[100];
	memcpy(msg, buf, count);
	msg[count] = 0;
	term_print(msg);
	return count;
}

void tty_putchar(struct tty_device *tty, int c)
{
	tty->buf[(tty->pos + tty->count) % TTY_BUF_SIZE] = c;
	if (tty->count != TTY_BUF_SIZE)
		tty->count++;
}
