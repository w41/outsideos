#ifndef FCNTL_H
#define FCNTL_H

/* newlib/_default_fcntl.h */

#define F_READ 0x0001
#define F_WRITE 0x0002
#define F_APPEND 0x0008
#define F_ASYNC 0x0040
#define F_EXLOCK 0x0100
#define F_CREAT 0x0200
#define F_TRUNC 0x0400
#define F_EXCL 0x0800
#define F_SYNC 0x2000
#define F_NONBLOCK 0x4000
#define F_NDELAY F_NONBLOCK
#define F_NOCTTY 0x8000

#define O_ACCMODE (O_RDONLY | O_WRONLY | O_RDWR)

#define O_RDONLY 0 /* +1 == FREAD */
#define O_WRONLY 1 /* +1 == FWRITE */
#define O_RDWR 2 /* +1 == FREAD|FWRITE */
#define O_APPEND F_APPEND
#define O_CREAT F_CREAT
#define O_TRUNC F_TRUNC
#define O_EXCL F_EXCL
#define O_SYNC F_SYNC
#define O_NONBLOCK F_NONBLOCK
#define O_NOCTTY F_NOCTTY

/* fcntl() commands */
#define F_DUPFD 0
#define F_GETFD 1
#define F_SETFD 2
#define F_GETFL 3
#define F_SETFL 4

#endif
