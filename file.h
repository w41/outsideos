#ifndef FILE_H
#define FILE_H

#include "fs/fat.h"
#include "idt.h"

#define S_IFMT 00170000
#define S_IFSOCK 0140000
#define S_IFLNK 0120000
#define S_IFREG 0100000
#define S_IFBLK 0060000
#define S_IFDIR 0040000
#define S_IFCHR 0020000
#define S_IFIFO 0010000
#define S_ISUID 0004000
#define S_ISGID 0002000
#define S_ISVTX 0001000

struct file {
	uint32_t pos;
	struct inode *inode;
	struct dentry *dentry;
	int fd;
	int flags;
};

struct stat {
	short st_dev; /* ID of device containing file */
	unsigned short st_ino; /* Inode number */
	int st_mode; /* File type and mode */
	unsigned short st_nlink; /* Number of hard links */
	unsigned short st_uid; /* User ID of owner */
	unsigned short st_gid; /* Group ID of owner */
	short st_rdev; /* Device ID (if special file) */
	long int st_size; /* Total size, in bytes */
	//	int st_blksize;     /* Block size for filesystem I/O */
	//	int  st_blocks;      /* Number of 512B blocks allocated */

	/* FIXME: Not filling this for now. */
	//struct timespec st_atim;  /* Time of last access */
	//struct timespec st_mtim;  /* Time of last modification */
	//struct timespec st_ctim;  /* Time of last status change */
};

struct dirent {
	unsigned long d_ino; /* Inode number */
	unsigned long d_off; /* Offset to next linux_dirent */
	unsigned short d_reclen; /* Length of this linux_dirent */
	char d_name[]; /* Filename (null-terminated) */
	/* length is actually (d_reclen - 2 -
			    offsetof(struct linux_dirent, d_name)) */
};

int sys_open(struct trapframe *tf);
int sys_close(struct trapframe *tf);
int sys_stat(struct trapframe *tf);
int sys_fstat(struct trapframe *tf);
int sys_write(struct trapframe *tf);
int sys_read(struct trapframe *tf);
int sys_lseek(struct trapframe *tf);
int sys_fcntl(struct trapframe *tf);
int sys_ioctl(struct trapframe *tf);
int sys_getdents(struct trapframe *tf);
int sys_chdir(struct trapframe *tf);

#endif
