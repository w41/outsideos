#include "syscall.h"
#include "exec.h"
#include "file.h"
#include "mm.h"
#include "sched.h"
#include "unistd.h"

/* ABI for syscalls:
 * %eax - syscall #, clobbered
 * %ebx - arg 0, clobbered
 * %ecx - arg 1, clobbered
 *
 * %eax - return value
 */
void syscall(struct trapframe *tf)
{
	static int (*fns[256])(struct trapframe * tf) = {
		[SYSCALL_exit] = &sys_exit,
		[SYSCALL_getpid] = &sys_getpid,
		[SYSCALL_fork] = &sys_fork,
		[SYSCALL_open] = &sys_open,
		[SYSCALL_close] = &sys_close,
		[SYSCALL_write] = &sys_write,
		[SYSCALL_read] = &sys_read,
		[SYSCALL_lseek] = &sys_lseek,
		[SYSCALL_stat] = &sys_stat,
		[SYSCALL_fstat] = &sys_fstat,
		[SYSCALL_fcntl] = &sys_fcntl,
		[SYSCALL_waitpid] = &sys_wait,
		[SYSCALL_getuid] = &sys_getuid,
		[SYSCALL_getgid] = &sys_getgid,
		[SYSCALL_geteuid] = &sys_geteuid,
		[SYSCALL_getegid] = &sys_getegid,
		[SYSCALL_getppid] = &sys_getppid,
		[SYSCALL_brk] = &sys_sbrk,
		[SYSCALL_ioctl] = &sys_ioctl,
		[SYSCALL_execve] = &sys_execve,
		[SYSCALL_getdents] = &sys_getdents,
		[SYSCALL_chdir] = &sys_chdir,
	};

	if (fns[tf->eax] == 0) {
		char msg[100];
		sprintf(msg, "Unimplemented POSIX syscall: %d", tf->eax);
		panic(msg);
	} else {
		tf->eax = fns[tf->eax](tf);
	}
}
