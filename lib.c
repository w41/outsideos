#include <stdarg.h>
#include <stdint.h>

#include "console.h"
#include "lib.h"

#define BUFSIZ 20

static int log(unsigned int num, int radix)
{
	int retval = 0;
	for (; num > 0; num /= radix)
		retval++;

	return retval;
}

int strlen(const char *str)
{
	char *c = str;
	while (*c)
		c++;
	return c - str;
}

static char *strcat(char *dest, const char *src)
{
	char *d = dest + strlen(dest);
	while (*d++ = *src++)
		;
	return dest;
}

char *strcpy(char *dest, const char *src)
{
	char *d = dest;
	while (*d++ = *src++)
		;
	return dest;
}

char *strncpy(char *dest, const char *src, int n)
{
	char *d = dest;
	while ((*d++ = *src++) && n-- > 0)
		;
	return dest;
}

char *strchr(const char *s, int c)
{
	while (*s && *s != c)
		s++;

	return !*s ? 0 : s;
}

int strcmp(const char *s1, const char *s2)
{
	do {
		if (*s1 == 0 && *s2 == 0)
			return 0;
		if (*s1 == 0 || *s1 < *s2)
			return 1;
		if (*s2 == 0 || *s1 > *s2)
			return -1;

		s1++;
		s2++;
	} while (1);
}

int strncmp(const char *s1, const char *s2, int n)
{
	while (n--) {
		if (*s1 == 0 && *s2 == 0)
			return 0;
		if (*s1 == 0 || *s1 < *s2)
			return 1;
		if (*s2 == 0 || *s1 > *s2)
			return -1;

		s1++;
		s2++;
	}

	return 0;
}

char *strdup(const char *s)
{
	int len = strlen(s);
	char *retval = kmalloc(len + 1);
	if (!retval)
		return 0;

	memcpy(retval, s, len);
	retval[len] = 0;

	return retval;
}

static char *itoa(int num, char *buf, int radix)
{
	static char digits[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	char *c = buf;
	int i, numdigits;
	if (num < 0 && radix == 10) {
		*c++ = '-';
		num *= -1;
	} else if (num == 0) {
		*c++ = '0';
		*c = 0;
		return buf;
	}

	unsigned int unum = (unsigned int)num;

	numdigits = log(unum, radix);
	for (i = numdigits - 1; i >= 0; i--) {
		c[i] = digits[unum % radix];
		unum /= radix;
	}

	c[numdigits] = 0;
	return buf;
}

int sprintf(char *str, const char *format, ...)
{
	va_list vargs;
	char buf[BUFSIZ];
	char *c = format;
	char *orig = str;
	int i;
	va_start(vargs, format);
	for (; *c != 0; *c++) {
		if (*c != '%') {
			*str++ = *c;
		} else {
			*c++;
			switch (*c) {
			case 'x':
			case 'X':
				itoa(va_arg(vargs, unsigned int), buf, 16);
				for (i = 0; buf[i]; i++)
					*str++ = buf[i];
				buf[0] = 0;
				break;
			case 'd':
				itoa(va_arg(vargs, int), buf, 10);
				for (i = 0; buf[i]; i++)
					*str++ = buf[i];
				buf[0] = 0;
				break;
			case 'c':
				*str++ = (char)va_arg(vargs, int);
				break;
			case 's':
				str = strcat(str, va_arg(vargs, char *));
				str += strlen(str);
				break;
			case 'p':
				itoa((uint32_t)va_arg(vargs, void *), buf, 16);
				for (i = 0; buf[i]; i++)
					*str++ = buf[i];
				break;
			}
		}
	}

	*str = 0;

	va_end(vargs);
	return str - orig;
}

unsigned int round_up_multiple(unsigned int num, unsigned int div)
{
	unsigned int rem = num % div;
	return rem == 0 ? num : num + div - rem;
}

unsigned int round_down_multiple(unsigned int num, unsigned int div)
{
	unsigned int rem = num % div;
	return rem == 0 ? num : num - rem;
}

void *memset(void *dest, int c, size_t n)
{
	for (int i = 0; i < n; i++)
		*(char *)(dest + i) = c;
	return dest;
}

void *memcpy(void *dest, const void *src, size_t n)
{
	for (int i = 0; i < n; i++)
		*(char *)(dest + i) = *(char *)(src + i);

	return dest;
}
