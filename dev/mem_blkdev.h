#ifndef MEM_BLKDEV_H
#define MEM_BLKDEV_H

struct mem_blkdev;

struct mem_blkdev *mem_blkdev_alloc(void *addr, int length);
int mem_blkdev_read(struct mem_blkdev *dev, int offset, int count, char *buf);
int mem_blkdev_write(struct mem_blkdev *dev, int offset, int count,
		     const char *buf);
void mem_blkdev_free(struct mem_blkdev *dev);

#endif
