#include "mem_blkdev.h"
#include "lib.h"

struct mem_blkdev {
	void *addr;
	int length;
};

struct mem_blkdev *mem_blkdev_alloc(void *addr, int length)
{
	struct mem_blkdev *dev = kmalloc(sizeof(struct mem_blkdev));
	if (!dev)
		return 0;

	dev->addr = addr;
	dev->length = length;

	return dev;
}

int mem_blkdev_read(struct mem_blkdev *dev, int offset, int count, char *buf)
{
	if (offset + count > dev->length)
		count = dev->length - offset;

	memcpy(buf, dev->addr + offset, count);

	return count;
}

int mem_blkdev_write(struct mem_blkdev *dev, int offset, int count,
		     const char *buf)
{
	if (offset + count > dev->length)
		count = dev->length - offset;

	memcpy(dev->addr + offset, buf, count);

	return count;
}

void mem_blkdev_free(struct mem_blkdev *dev)
{
	kfree(dev);
}
