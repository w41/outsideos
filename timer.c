#include "timer.h"

void timer_init()
{
	outb(PIT_CMD, 0x36); /* CH 0, BCD 0, Square wave */
	outb(PIT_CH0, (PIT_HZ / HZ) & 0xff);
	outb(PIT_CH0, (PIT_HZ / HZ) >> 8);
}
