#include <stddef.h>
#include <stdint.h>

#include "console.h"
#include "mm.h"
#include "ports.h"

// This is the x86's VGA textmode buffer. To display text, we write data to this memory location
volatile uint16_t *vga_buffer = pa2va((uint16_t *)0xB8000);
// By default, the VGA textmode buffer has a size of 80x25 characters
const int VGA_COLS = 80;
const int VGA_ROWS = 25;

// We start displaying text in the top-left of the screen (column = 0, row = 0)
int term_col = 0;
int term_row = 0;
uint8_t term_color = 0x0F; // Black background, White foreground

static void update_cursor(int row, int col)
{
	uint16_t pos = row * VGA_COLS + col;
	outb(0x3d4, 0x0f);
	outb(0x3d5, (uint8_t)(pos & 0xff));
	io_wait();
	outb(0x3d4, 0x0e);
	outb(0x3d5, (uint8_t)(pos >> 8));
}

// This function initiates the terminal by clearing it
void term_init()
{
	// Clear the textmode buffer
	for (int col = 0; col < VGA_COLS; col++) {
		for (int row = 0; row < VGA_ROWS; row++) {
			// The VGA textmode buffer has size (VGA_COLS * VGA_ROWS).
			// Given this, we find an index into the buffer for our character
			const size_t index = (VGA_COLS * row) + col;
			// Entries in the VGA buffer take the binary form BBBBFFFFCCCCCCCC, where:
			// - B is the background color
			// - F is the foreground color
			// - C is the ASCII character
			vga_buffer[index] =
				((uint16_t)term_color << 8) |
				' '; // Set the character to blank (a space character)
		}
	}
}

// This function places a single character onto the screen
void term_colored_putc(char c, uint8_t color)
{
	int i, j;
	// Remember - we don't want to display ALL characters!
	switch (c) {
	case '\n': // Newline characters should return the column to 0, and increment the row
	{
		term_row++;
		term_col = 0;
		break;
	}

	default: // Normal characters just get displayed and then increment the column
	{
		const size_t index =
			(VGA_COLS * term_row) +
			term_col; // Like before, calculate the buffer index
		vga_buffer[index] = (color << 8) | c;
		term_col++;
		break;
	}
	}

	// What happens if we get past the last column? We need to reset the column to 0, and increment the row to get to a new line
	if (term_col >= VGA_COLS) {
		term_col = 0;
		term_row++;
	}

	if (term_row == VGA_ROWS) {
		for (i = 0; i < VGA_ROWS - 1; i++)
			for (j = 0; j < VGA_COLS; j++) {
				const size_t index_to = (VGA_COLS * i) + j;
				const size_t index_from =
					(VGA_COLS * (i + 1)) + j;
				vga_buffer[index_to] = vga_buffer[index_from];
			}
		term_row = VGA_ROWS - 1;
		for (int j = 0; j < VGA_COLS; j++)
			vga_buffer[VGA_COLS * (VGA_ROWS - 1) + j] =
				((uint16_t)term_color << 8) | ' ';
	}

	update_cursor(term_row, term_col);
}

void term_putc(char c)
{
	term_colored_putc(c, COLOR_WHITE);
}

void term_colored_print(const char *str, uint8_t color)
{
	for (size_t i = 0; str[i] != '\0';
	     i++) // Keep placing characters until we hit the null-terminating character ('\0')
		term_colored_putc(str[i], color);
}

// This function prints an entire string onto the screen
void term_print(const char *str)
{
	term_colored_print(str, COLOR_WHITE);
}

void term_print_ok(const char *str)
{
	term_colored_print("[OK] ", COLOR_GREEN);
	term_print(str);
	term_print("\n");
}

void term_print_info(const char *str)
{
	term_colored_print("[INFO] ", COLOR_YELLOW);
	term_print(str);
	term_print("\n");
}

void term_print_error(const char *str)
{
	term_colored_print("[ERROR] ", COLOR_RED);
	term_print(str);
	term_print("\n");
}

void term_print_panic(const char *str)
{
	term_colored_print("[PANIC] ", COLOR_RED);
	term_print(str);
	term_print("\n");
}
void term_print_debug(const char *str)
{
	term_colored_print("[DEBUG] ", COLOR_GRAY);
	term_print(str);
	term_print("\n");
}
