#include <stdint.h>

#include "mm.h"

#include "console.h"
#include "idt.h"
#include "lib.h"
#include "multiboot.h"
#include "panic.h"
#include "sched.h"

#include "dev/mem_blkdev.h"

/* all available memory (1M pages by sizeof(uint32_t)) */
#define BITMAP_SIZE (0x100000000ULL / PAGE_SIZE / (8 * sizeof(void *)))

/* x32's two-level indirection (page dir -> page table -> page) */
#define PD_ENTRIES (PAGE_SIZE / sizeof(void *))
#define PT_ENTRIES (PAGE_SIZE / sizeof(void *))

/* macros for conversion of addresses to offsets etc. */

/* discard bottom 12 bits (4K) */
#define ADDR_TO_PFN(addr) ((uint32_t)addr >> 12)

/* discard additional 5 bits (bitmap elements correspond to 32 pages) */
#define ADDR_TO_BITMAP_OFFSET(addr) (ADDR_TO_PFN(addr) >> 5)

/* position of page within its 32-bit element */
#define ADDR_TO_BITMAP_ELEMENT_INDEX(addr) (ADDR_TO_PFN(addr) % 32)

/* drop bottom 10 bits from the PFN */
#define ADDR_TO_PGDIR_OFFSET(addr) (ADDR_TO_PFN(addr) >> 10)

extern uint32_t KERNEL_START;

static struct mmap_region {
	uint32_t mem_addr;
	uint32_t mem_size;
} mmap[256];
static uint8_t mmap_nr_regions;

struct bios_mmap_entry {
	uint32_t size;
	uint32_t base_addr_low, base_addr_high;
	uint32_t length_low, length_high;
	uint32_t type;
};

/* Page directory takes exactly 1 page of space (1K of 4-byte entries) */
uint32_t page_directory[PD_ENTRIES] __attribute__((aligned(PAGE_SIZE)));

/* First page table is fixed and is always a [0..4M) -> [0..4M) identity mapping */
static uint32_t first_page_table[PT_ENTRIES]
	__attribute__((aligned(PAGE_SIZE)));

static uint32_t first_free_page_table[PT_ENTRIES]
	__attribute((aligned(PAGE_SIZE)));

/* Two bitmaps covering the whole 32-bit physical space:
 * available_pages_bitmap - statically fixed at bootup (detect_memory())
 * free_pages_bitmap - pages that are available to physical MM
 */
static uint32_t available_pages_bitmap[BITMAP_SIZE];
static uint32_t free_pages_bitmap[BITMAP_SIZE];

static struct page_table {
	int present;
	uint32_t vaddr;
	uint32_t paddr;
} page_tables[PD_ENTRIES];
static int allocated_page_tables_count;
static int used_page_tables_count;

static uint32_t kernel_heap_first_address;

extern uint32_t kernel_end;

static int paging_enabled;

struct human_readable {
	char str[9];
};
static struct human_readable to_human_readable(uint32_t length)
{
	struct human_readable retval;
	char base;
	int num;
	if (length >= 0x100000) {
		base = 'M';
		num = length / 0x100000;
	} else if (length >= 0x1000) {
		base = 'K';
		num = length / 0x1000;
	} else {
		base = 'B';
		num = length;
	}

	sprintf(retval.str, "%d%c", num, base);
	return retval;
}

static void mark_range_used(uint32_t start, uint32_t end)
{
	uint32_t page_start = ADDR_TO_PFN(round_down_multiple(start, 4096));
	uint32_t page_end = ADDR_TO_PFN(round_up_multiple(end, 4096));

	if (page_start % 32 != 0) {
		free_pages_bitmap[page_start / 32] &=
			~((1U << (32 - page_start % 32)) - 1);
		page_start += 32 - (page_start % 32);
	}
	if (page_end % 32 != 0) {
		free_pages_bitmap[page_end / 32] &= (1U << (page_end % 32)) - 1;
		page_end -= page_end % 32;
	}

	memset(free_pages_bitmap + page_start / 32, 0,
	       (page_end - page_start) / 8);
}

static int detect_memory(multiboot_info_t *mbinfo)
{
	int offset = 0;

	uint32_t mmap_length = mbinfo->mmap_length;
	uint32_t mmap_addr = mbinfo->mmap_addr;
	uint32_t i;

	{
		char str[100] = "";
		sprintf(str, "Detected kernel at 0x%x-0x%x (%s)", 1 << 20,
			va2pa(&kernel_end),
			to_human_readable(va2pa(&kernel_end) - (1 << 20)).str);
		term_print_info(str);
	}

	while (offset < mmap_length) {
		struct bios_mmap_entry *entry =
			(struct bios_mmap_entry *)(mbinfo->mmap_addr + offset);

		for (i = round_up_multiple(entry->base_addr_low, PAGE_SIZE);
		     i < round_down_multiple(entry->base_addr_low +
						     entry->length_low,
					     PAGE_SIZE);
		     i += PAGE_SIZE) {
			available_pages_bitmap[ADDR_TO_BITMAP_OFFSET(i)] |=
				(1 << ADDR_TO_BITMAP_ELEMENT_INDEX(i));
		}

		if (entry->type == 1) {
			mmap[mmap_nr_regions++] = (struct mmap_region){
				entry->base_addr_low, entry->length_low};
			char str[100] = "";
			sprintf(str, "Detected RAM region at 0x%x-0x%x (%s)",
				entry->base_addr_low,
				entry->base_addr_low + entry->length_low,
				to_human_readable(entry->length_low).str);
			term_print_info(str);
		}

		offset += entry->size + sizeof(entry->size);
	}

	kernel_heap_first_address =
		round_up_multiple(va2pa(&kernel_end), PAGE_SIZE);

	/* detect initrd and other modules */
	for (int i = 0; i < mbinfo->mods_count; i++) {
		struct module *mod = (struct module *)mbinfo->mods_addr + i;
		char str[100] = "";
		sprintf(str, "Detected multiboot module at 0x%x-0x%x (%s)",
			mod->mod_start, mod->mod_end,
			to_human_readable(mod->mod_end - mod->mod_start).str);
		term_print_info(str);

		/* initrd memory *currently* not available for dynamic allocation */
		/* FIXME: make it so! */
		memset(available_pages_bitmap +
			       ADDR_TO_BITMAP_OFFSET(mod->mod_start),
		       0,
		       4 * (ADDR_TO_BITMAP_OFFSET(mod->mod_end) -
			    ADDR_TO_BITMAP_OFFSET(mod->mod_start)));
		kernel_heap_first_address =
			round_up_multiple(mod->mod_end, PAGE_SIZE);
	}

	/* memory at [0..kernel_end] not available for dynamic allocation */
	memset(available_pages_bitmap, 0,
	       round_up_multiple(
		       round_up_multiple(va2pa(&kernel_end), PAGE_SIZE) >> 15,
		       4));

	/* mark all pages as free */
	memset(free_pages_bitmap, 0xffffffff, BITMAP_SIZE * sizeof(uint32_t));

	return 0;
}

static int init_mem_devices(multiboot_info_t *mbinfo)
{
	for (int i = 0; i < ((multiboot_info_t *)pa2va(mbinfo))->mods_count;
	     i++) {
		char header[5];
		char str[100] = "";
		struct module *mod = (struct module *)(pa2va(
					     ((multiboot_info_t *)pa2va(mbinfo))
						     ->mods_addr)) +
				     i;

		struct mem_blkdev *initrd = mem_blkdev_alloc(
			pa2va(mod->mod_start), mod->mod_end - mod->mod_start);
		struct superblock *sb = create_from_device(initrd);
		sprintf(str, "detected sb: 0x%x", sb);
		term_print_info(str);
	}

	return 0;
}

static void *get_page_in_range(uint32_t start, uint32_t end)
{
	if (!paging_enabled)
		return 0;

	void *retval = 0;
	uint32_t free_page_word;
	for (int i = ADDR_TO_BITMAP_OFFSET(start);
	     i <= ADDR_TO_BITMAP_OFFSET(end); i++) {
		if (!available_pages_bitmap[i] || !free_pages_bitmap[i])
			continue;
		if (!(available_pages_bitmap[i] & free_pages_bitmap[i]))
			continue;
		free_page_word = available_pages_bitmap[i] &
				 free_pages_bitmap[i];
		free_pages_bitmap[i] &=
			~(1 << (31 - __builtin_clz(free_page_word)));
		retval = (void *)(PAGE_SIZE *
				  (32 * i + __builtin_clz(free_page_word)));
		break;
	}

#ifdef MM_DEBUG
	char msg[100];
	sprintf(msg, "get_page() -> %x", (uint32_t)retval);
	term_print_debug(msg);
#endif

	if (!retval)
		panic("Out of memory!");

	return retval;
}

void *get_page(void)
{
	return get_page_in_range(0, 0xFFFFFFFF);
}

void put_page(void *addr)
{
#ifdef MM_DEBUG
	char msg[100];
	sprintf(msg, "put_page(%x) -> void", (uint32_t)addr);
	term_print_debug(msg);
#endif
	free_pages_bitmap[ADDR_TO_BITMAP_OFFSET(addr)] |=
		1 << ADDR_TO_BITMAP_ELEMENT_INDEX(addr);
}

static int setup_paging(void)
{
	/* Destroy the identity mapping and map the whole 1G to (3..4G)
	 * only above the 3G mark.
	 * */
	for (int i = 0; i < PT_ENTRIES; i++)
		first_page_table[i] = (i * 0x1000) | 7;
	page_directory[ADDR_TO_PGDIR_OFFSET(&KERNEL_START)] =
		va2pa(first_page_table) | 7;

	/* Set aside 4M for all the page tables. */
	kernel_heap_first_address =
		round_up_multiple(kernel_heap_first_address, 1024 * 4096);

	for (int i = 0; i < 1024; i++)
		first_free_page_table[i] =
			(kernel_heap_first_address + i * 0x1000) | 7;
	page_directory[ADDR_TO_PGDIR_OFFSET((uint32_t)&KERNEL_START +
					    kernel_heap_first_address)] =
		va2pa(first_free_page_table) | 7;

	page_directory[0] = 0;

	asm volatile("mov %0, %%eax" : : "r"(va2pa(page_directory)));
	asm volatile("mov %eax, %cr3");

	/* We have enabled paging in boot.S already. */
	paging_enabled = 1;

	/* Allocate pages for page tables (one for every 1024 pages past 4M). */
	for (int i = 1024 / 32; i < BITMAP_SIZE; i += 1024 / 32) {
		int alloc_pt = 0;
		for (int j = 0; j < 32; j++) {
			if (available_pages_bitmap[i + j]) {
				alloc_pt = 1;
				break;
			}
		}

		page_tables[allocated_page_tables_count++].paddr =
			(uint32_t)get_page_in_range(
				kernel_heap_first_address,
				round_up_multiple(kernel_heap_first_address,
						  1024 * 4096));
		kernel_heap_first_address += 4096;
	}

	kernel_heap_first_address = pa2va(kernel_heap_first_address);

	term_print_ok("Paging initialized.");
	return 0;
}

static void handle_user_pf(uint32_t addr)
{
	struct mapping *mapping = current_task->mapping;
	uint32_t *page_table;
	while (mapping != 0) {
		if (addr >= mapping->region.va &&
		    addr < mapping->region.va + mapping->region.length) {
			if (*((uint32_t *)(pa2va(current_task->page_directory)) +
			      ADDR_TO_PGDIR_OFFSET(addr)) == 0) {
				page_table = get_page();
				*((uint32_t *)(pa2va(
					  current_task->page_directory)) +
				  ADDR_TO_PGDIR_OFFSET(addr)) =
					(uint32_t)page_table | 0x7;
			} else {
				page_table = (uint32_t *)*(
					(uint32_t *)(pa2va(
						current_task->page_directory)) +
					ADDR_TO_PGDIR_OFFSET(addr));
			}

			*((uint32_t *)pa2va((uint32_t)page_table & 0xFFFFFC00) +
			  ((addr >> 12) & 0x3FF)) =
				(mapping->region.pa +
				 ((addr - mapping->region.va) & 0xFFFFFC00)) |
				0x7;
			return;
		}

		mapping = mapping->next;
	}

	/* Traverse mapping and try to figure out which segment the PF might belong to. */
}
void handle_page_fault(uint32_t err)
{
	uint32_t addr;
	asm volatile("mov %cr2, %eax");
	asm volatile("mov %%eax, %0" : "=r"(addr));

#ifdef MM_DEBUG
	char msg[100];
	sprintf(msg, "Page fault at 0x%x", addr);
	term_print_info(msg);
#endif
	if (addr < 0x1000)
		panic("Page fault in first page - null pointer dereference.");

	if (addr < (uint32_t)&KERNEL_START) {
		handle_user_pf(addr);
	} else {
		/* When getting here from a process context, first try to steal the entry from the kernel page directory. */
		if (current_task &&
		    page_directory[ADDR_TO_PGDIR_OFFSET(addr)]) {
			*(((uint32_t *)pa2va(current_task->page_directory)) +
			  ADDR_TO_PGDIR_OFFSET(addr)) =
				page_directory[ADDR_TO_PGDIR_OFFSET(addr)];
		} else {
			page_tables[used_page_tables_count].vaddr =
				addr & ~((1 << 12) - 1);
			for (int i = 0; i < PT_ENTRIES; i++) {
				*(uint32_t *)(pa2va(page_tables
							    [used_page_tables_count]
								    .paddr) +
					      i * 4) =
					((addr & 0x0fc00000) + 0x1000 * i) |
					0x7;
			}

			page_directory[ADDR_TO_PGDIR_OFFSET(addr)] =
				page_tables[used_page_tables_count].paddr | 0x7;

			used_page_tables_count++;
		}
	}
}

/* Kernel heap implementation. */
/* Set kernel heap size = 1/8 of available memory. */
/* Assume the heap starts at 4M mark. */

/* Split that in the following way: 
 * 1/64 - 8 byte objects
 * 1/32 - 16 byte objects
 * 1/8 - 32 byte objects
 * 1/4 - 128 byte objects
 * 1/2 - 1024 byte objects
 * 5/64 - 4K byte objects
 */

struct arena_bitmap {
	uint32_t next;
	uint32_t bitmap[1023];
};

struct arena {
	int order;
	struct {
		int n;
		int d;
	};
	int num_objects;
	int num_free_objects;
	uint32_t bitmap;
	uint32_t bitmap_num_pages;
	uint32_t first_address;
} arenas[6] = {{3, 1, 64}, {4, 1, 32}, {5, 1, 8},
	       {7, 1, 4},  {10, 1, 2}, {12, 5, 64}};

static void *get_free_object(struct arena *arena)
{
	struct arena_bitmap *bitmap = (struct arena_bitmap *)arena->bitmap;

	for (int i = 0; i < arena->bitmap_num_pages; i++) {
		for (int j = 0; j < sizeof(bitmap->bitmap) / sizeof(uint32_t);
		     j++) {
			if (bitmap->bitmap[j] == 0xFFFFFFFF)
				continue;

			short bit = __builtin_ctz(~bitmap->bitmap[j]);
			bitmap->bitmap[j] |= 1 << bit;
			arena->num_free_objects--;
			return (void *)arena->first_address +
			       ((i * 1023 + j * 32 + bit) << arena->order);
		}

		bitmap = (struct arena_bitmap *)bitmap->next;
	}

	return 0;
}

void *kmalloc(size_t size)
{
	struct arena *arena;
	struct arena_bitmap *bitmap;
	for (int i = 0; i < sizeof(arenas) / sizeof(struct arena); i++) {
		if (size <= (1 << arenas[i].order) &&
		    arenas[i].num_free_objects > 0) {
			arena = arenas + i;
			break;
		}
	}

	if (!arena) {
		term_print_error(
			"kmalloc(): object too big or not enough memory");
		return 0;
	}

	void *obj = get_free_object(arena);
	if (!obj)
		panic("Internal error in get_free_object().");

	return obj;
}

void *kzmalloc(size_t size)
{
	void *retval = kmalloc(size);
	if (retval)
		memset(retval, 0, size);

	return retval;
}

void kfree(void *addr)
{
	struct arena *arena;
	for (int i = sizeof(arenas) / sizeof(struct arena) - 1; i >= 0; i--) {
		if ((uint32_t)addr >= arenas[i].first_address) {
			arena = arenas + i;
			break;
		}
	}

	if (!arena) {
		term_print_error(
			"kfree(): object does not belong to kernel heap");
		return;
	}

	int idx = ((uint32_t)addr - arena->first_address) >> arena->order;
	struct arena_bitmap *bitmap = (struct arena_bitmap *)arena->bitmap;
	while (idx >= sizeof(bitmap->bitmap) * 8) {
		bitmap = (struct arena_bitmap *)bitmap->next;
		idx -= sizeof(bitmap->bitmap) * 8;
	}

	if (~bitmap->bitmap[idx / 32] & (1 << idx % 32))
		panic("kfree(): double free");

	bitmap->bitmap[idx / 32] &= ~(1 << idx % 32);
	arena->num_free_objects++;
}

static int init_kernel_heap()
{
	uint32_t first_address = kernel_heap_first_address;
	/* FIXME: hardcoding to 10M, need to determine size more intelligently */
	uint32_t last_address = first_address + 1024 * 1024 * 10;
	//		((mmap[mmap_nr_regions-1].mem_addr + mmap[mmap_nr_regions-1].mem_size - first_address) >> 3);
	uint32_t next_arena_first_address = first_address;
	for (int i = 0; i < sizeof(arenas) / sizeof(struct arena); i++) {
		arenas[i].first_address = next_arena_first_address;
		arenas[i].num_free_objects = arenas[i].num_objects =
			(last_address - first_address) / 2 * arenas[i].n /
			arenas[i].d / (1 << arenas[i].order);
		arenas[i].bitmap_num_pages =
			arenas[i].num_objects / (1023 * sizeof(uint32_t) * 8) +
			1;
		next_arena_first_address +=
			arenas[i].num_objects * (1 << arenas[i].order);
		mark_range_used(va2pa(arenas[i].first_address),
				va2pa(next_arena_first_address));
		uint32_t prev_bitmap;
		for (int j = arenas[i].bitmap_num_pages - 1; j >= 0; j--) {
			struct arena_bitmap *bitmap = pa2va(get_page());
			if (!bitmap)
				panic("Not enough memory for kernel heap!");
			memset(bitmap, 0, sizeof(struct arena_bitmap));
			if (j != arenas[i].bitmap_num_pages - 1)
				bitmap->next = prev_bitmap;

			prev_bitmap = (uint32_t)bitmap;
		}
		arenas[i].bitmap = prev_bitmap;
	}

	term_print_ok("Kernel heap initialized.");
#ifdef MM_DEBUG
	char msg[200] = {0};
	char *cur = msg;
	uint32_t meta_size = 0;
	uint32_t total_size = 0;

	cur += sprintf(cur, "Kernel heap starts at: %x\n",
		       kernel_heap_first_address);
	cur += sprintf(cur, "Kernel heap object size: ");
	for (int i = 0; i < sizeof(arenas) / sizeof(struct arena); i++) {
		uint32_t size = arenas[i].num_objects * (1 << arenas[i].order);
		cur += sprintf(cur, "%d -> %s%s", arenas[i].order,
			       to_human_readable(size).str,
			       i != sizeof(arenas) / sizeof(struct arena) - 1 ?
				       ", " :
				       "");
		meta_size += PAGE_SIZE * arenas[i].bitmap_num_pages;
		total_size += size;
	}
	cur += sprintf(cur, " (%s total)", to_human_readable(total_size).str);
	term_print_debug(msg);

	char msg2[100] = {0};
	sprintf(msg2, "Kernel heap meta size: %s",
		to_human_readable(meta_size).str);
	term_print_debug(msg2);
#endif
	return 0;
}

int mm_init(multiboot_info_t *mbinfo)
{
	int retval = detect_memory(mbinfo);
	if (retval)
		goto error;

	retval = setup_paging();
	if (retval)
		goto error;

	retval = init_kernel_heap();
	if (retval)
		goto error;

	retval = init_mem_devices(mbinfo);
	if (retval)
		goto error;

	term_print_ok("Memory subsystem initialized.");
	return 0;
error:
	term_print_error("Memory subsystem initialization error.");
	return 1;
}

int sys_sbrk(struct trapframe *tf)
{
	int bytes = (int)tf->ebx;
	if (bytes == 0) {
		return current_task->brk;
	}

	/* FIXME: Only increasing the break for now. */
	uint32_t retval = current_task->brk;
	/* Last N elements are all MAPPING_HEAP, find the last one and
	 * try to extend it. */
	for (struct mapping *mapping = current_task->mapping; mapping;
	     mapping = mapping->next) {
		if (!mapping->next && mapping->type == MAPPING_HEAP) {
			/* Use remainder of the current page. */
			bytes -= 4096 - (current_task->brk % 4096);
			/* Allocate pages for the rest (potentially including
			 * the last page which will remain unused for now).
			 * */
			while (bytes >= 0) {
				void *page = get_page();
				if (page != mapping->region.pa +
						    mapping->region.length) {
					struct mapping *heap = kzmalloc(
						sizeof(struct mapping));
					heap->region.va =
						mapping->region.va +
						mapping->region.length;
					heap->region.pa = page;
					heap->region.length = 4096;
					heap->type = MAPPING_HEAP;
					add_mapping(current_task, heap);
				} else {
					mapping->region.length += 4096;
				}
				bytes -= 4096;
			}

			break;
		}
	}

	current_task->brk += tf->ebx;
	return retval;
}
