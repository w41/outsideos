#include "file.h"
#include "console.h"
#include "errno.h"
#include "fcntl.h"
#include "fs/fat_dentry.h"
#include "lib.h"
#include "mm.h"
#include "sched.h"
#include "tty.h"

#include "termios.h"

int sys_open(struct trapframe *tf)
{
	struct inode *inode;

	int i = 0;
	for (; i < MAX_FILES; i++) {
		if (!current_task->files[i])
			break;
	}

	if (i == MAX_FILES)
		return -EMFILE;

	struct file *file = kmalloc(sizeof(struct file));
	if (!file)
		return -ENOMEM;

	/* FIXME: handle a bunch of special entries. */
	if (!strcmp((const char *)tf->ebx, "/dev/tty")) {
		file->inode = tty_inode;
		current_task->files[i] = file;
		return i;
	} else {
		/* Filesystem entry. */
		struct dentry *dentry =
			path_lookup((const char *)tf->ebx, tf->ecx);
		if (!dentry) {
			kfree(file);
			return -ENOENT;
		}

		file->dentry = dentry;
		file->inode = dentry->inode;
	}

	file->pos = 0;
	file->fd = i;
	current_task->files[i] = file;

	return i;
}

int sys_close(struct trapframe *tf)
{
	if (tf->ebx >= MAX_FILES || current_task->files[tf->ebx] == 0)
		return -EBADF;

	kfree(current_task->files[tf->ebx]);
	current_task->files[tf->ebx] = 0;
	return 0;
}

int sys_fstat(struct trapframe *tf)
{
	unsigned int fd = tf->ebx;
	struct stat *fstat = (struct stat *)tf->ecx;

	if (tf->ebx >= MAX_FILES || current_task->files[tf->ebx] == 0)
		return -EBADF;

	memset(fstat, 0, sizeof(struct stat));

	struct inode *inode = current_task->files[tf->ebx]->inode;
	fstat->st_ino = inode->inode_no;
	fstat->st_size = inode->size;
	fstat->st_mode = inode->mode;
	return 0;
}

int sys_write(struct trapframe *tf)
{
	struct file *file;
	if (tf->ebx >= MAX_FILES || !(file = current_task->files[tf->ebx]))
		return -EBADF;

	if (file->inode == tty_inode)
		return tty_write(current_task->tty, (void *)tf->ecx, tf->edx);
	else
		/* FIXME: Filesystem write */
		return -EINVAL;

	//struct inode *inode = current_task->files[tf->ebx]->inode;
}

int sys_read(struct trapframe *tf)
{
	struct file *file;
	if (tf->ebx >= MAX_FILES || !(file = current_task->files[tf->ebx]))
		return -EBADF;

	if (file->inode == tty_inode)
		return tty_read(current_task->tty, (void *)tf->ecx, tf->edx);

	else
		/* FIXME: Filesystem read */
		return -EINVAL;
}

int sys_lseek(struct trapframe *tf)
{
	return -1;
}

int sys_stat(struct trapframe *tf)
{
	const char *path = (const char *)tf->ebx;
	struct stat *stat = (struct stat *)tf->ecx;
	memset(stat, 0, sizeof(struct stat));

	struct dentry *dentry = path_lookup(path, 0);
	if (!dentry)
		return -ENOENT;

	stat->st_ino = dentry->inode->inode_no;
	stat->st_size = dentry->inode->size;
	stat->st_uid = 0;
	stat->st_gid = 0;
	stat->st_mode = dentry->inode->mode;

	return 0;
}

int sys_fcntl(struct trapframe *tf)
{
	struct file *file;
	if (tf->ebx >= MAX_FILES || !(file = current_task->files[tf->ebx]))
		return -EBADF;

	switch (tf->ecx) {
	case F_GETFL:
		return file->flags;
	case F_SETFL:
		file->flags = (file->flags & (O_ACCMODE | O_APPEND | O_CREAT |
					      O_TRUNC | O_EXCL | O_NOCTTY)) |
			      (tf->edx & ~(O_ACCMODE | O_APPEND | O_CREAT |
					   O_TRUNC | O_EXCL | O_NOCTTY));
	default:
		return -EINVAL;
	}
}

int sys_ioctl(struct trapframe *tf)
{
	struct file *file;
	if (tf->ebx >= MAX_FILES || !(file = current_task->files[tf->ebx]))
		return -EBADF;

	if (file->inode != tty_inode)
		return -ENOTTY;

	switch (tf->ecx) {
	case _XCGETA:
		struct termios *termios = (struct termios *)tf->edx;
		memset(termios, 0, sizeof(struct termios));
		termios->c_lflag = ECHO;
		return 0;
	default:
		return -EINVAL;
	}
}

int sys_getdents(struct trapframe *tf)
{
	struct file *file;
	if (tf->ebx >= MAX_FILES || !(file = current_task->files[tf->ebx]))
		return -EBADF;

	if (!(file->inode->flags & I_DIRECTORY))
		return -ENOTDIR;

	struct dentry *dentry = file->dentry;
	struct dirent *dirent = (struct dirent *)tf->ecx;
	int count = tf->edx;
	int current_size = 0;
	for (struct dentry_children_list *child = dentry->children_list;
	     child != 0; child = child->next) {
		int namelen = child->dentry->name->length;
		int reclen = sizeof(struct dirent) + namelen + 1;
		if (current_size + reclen > count)
			return -EINVAL;

		dirent->d_ino = child->dentry->inode->inode_no;
		dirent->d_off = reclen - sizeof(unsigned long);
		dirent->d_reclen = reclen;
		memcpy(dirent->d_name, child->dentry->name->shortname,
		       min(namelen, DENTRY_NAME_EMBEDDED_SIZE));
		if (child->dentry->name->allocated_size > 0)
			memcpy(dirent->d_name + DENTRY_NAME_EMBEDDED_SIZE,
			       child->dentry->name->rest_of_name,
			       child->dentry->name->allocated_size);

		dirent->d_name[namelen] = 0;

		dirent = (void *)dirent + reclen;
		current_size += reclen;
	}

	/* Trailing null entry required by readdir(). */
	dirent->d_ino = 0;
	dirent->d_off = 0;
	dirent->d_reclen = 0;
	current_size += sizeof(struct dirent);

	return current_size;
}

/* FIXME: dentry refcounting */
int sys_chdir(struct trapframe *tf)
{
	const char *path = (const char *)tf->ebx;
	struct dentry *dentry = path_lookup(path, 0);
	if (!dentry)
		return -ENOENT;

	current_task->cwd = dentry;
	return 0;
}
