#include <stdint.h>

#include "elf.h"
#include "errno.h"
#include "fs/fat.h"
#include "fs/fat_dentry.h"
#include "lib.h"
#include "mm.h"
#include "sched.h"

int execve(const char *pathname, char *const argv[], char *const envp[])
{
	struct dentry *dentry = path_lookup(pathname, 0);
	if (!dentry)
		return -ENOENT;

	struct Elf32_Ehdr header;
	if (read_inode(dentry->inode, (char *)&header, 0, sizeof(header)) !=
	    sizeof(header))
		return -ENOEXEC;

	if (header.e_ident[0] != 0x7f || header.e_ident[1] != 'E' ||
	    header.e_ident[2] != 'L' || header.e_ident[3] != 'F')
		return -ENOEXEC;

	/* Only supporting 32-bit LSB ELF for now. */
	if (header.e_ident[4] != 1 || header.e_ident[5] != 1)
		return -ENOEXEC;

	/* Filter out non-executable ELF files. */
	if (header.e_type != 2)
		return -ENOEXEC;

	/* Filter out non-i386 ELF files. */
	if (header.e_machine != 3)
		return -ENOEXEC;

	current_task->state = TASK_DEAD;
	struct task *task = create_task(
		(void (*)(void))header.e_entry, argv, envp, TASK_USER,
		current_task->parent ? current_task->parent : 0, 0);
	struct mapping *mapping;

	/* Traverse the ELF program header. */
	for (int i = 0; i < header.e_phnum; i++) {
		struct Elf32_Phdr pheader;
		if (read_inode(dentry->inode, (char *)&pheader,
			       header.e_phoff + i * header.e_phentsize,
			       header.e_phentsize) != header.e_phentsize)
			return -ENOEXEC;

		/* FIXME: Only handling PT_LOAD segments for now. */
		if (pheader.p_type != 1)
			continue;

		/* FIXME: Does not handle the case where segments are not aligned
		 * to a page boundary.
		 */
		int offset = 0;
		mapping = kzmalloc(sizeof(struct mapping));
		mapping->region.va = pheader.p_vaddr - pheader.p_offset % 4096;
		mapping->region.pa = 0;
		mapping->region.length = pheader.p_offset % 4096;
		while (offset < pheader.p_memsz) {
			/* memsz can be greater than filesz, in which case we should add
			 * (memsz - filesz) trailing zeros.
			 */
			void *page = get_page();
			if (mapping->region.pa == 0) {
				memset(pa2va(page), 0, pheader.p_offset % 4096);
				offset += pheader.p_offset % 4096;
				pheader.p_offset -= pheader.p_offset % 4096;
				pheader.p_memsz += pheader.p_offset % 4096;
				pheader.p_filesz += pheader.p_offset % 4096;
			}

			int memsz = min(4096 - offset % 4096,
					pheader.p_memsz - offset);
			int filesz = pheader.p_filesz < offset ?
					     0 :
					     min(4096 - offset % 4096,
						 pheader.p_filesz - offset);

			if (filesz > 0) {
				read_inode(dentry->inode,
					   pa2va(page + offset % 4096),
					   pheader.p_offset + offset, filesz);
				memsz -= filesz;
			}

			if (memsz > 0) {
				memset(pa2va(page + filesz), 0, memsz);
			}

			if (mapping->region.pa == 0) {
				mapping->region.pa = page;
				mapping->region.length += filesz + memsz;
			} else if (mapping->region.pa +
					   mapping->region.length ==
				   page) {
				mapping->region.length += filesz + memsz;
			} else {
				add_mapping(task, mapping);
				mapping = kzmalloc(sizeof(struct mapping));
				mapping->region.pa = page;
				mapping->region.va = pheader.p_vaddr + offset;
				mapping->region.length = filesz + memsz;
			}

			offset += filesz + memsz;
		}

		add_mapping(task, mapping);
	}

	/* Add heap mapping after the PT_LOAD segments. */
	struct mapping *heap = kzmalloc(sizeof(struct mapping));
	heap->type = MAPPING_HEAP;
	heap->region.pa = get_page();
	heap->region.va =
		round_down_multiple(mapping->region.va + mapping->region.length,
				    4096) +
		4096;
	heap->region.length = 4096;
	task->brk = heap->region.va;
	add_mapping(task, heap);

	task->pid = current_task->pid;
	task->ppid = current_task->ppid;
	asm volatile("sti");
	while (1) {
	}
}

int sys_execve(struct trapframe *tf)
{
	return execve((const char *)tf->ebx, (const char **)tf->ecx,
		      (const char **)tf->edx);
}
