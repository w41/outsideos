#ifndef CONSOLE_H
#define CONSOLE_H

#define COLOR_GRAY 0x7
#define COLOR_GREEN 0xA
#define COLOR_RED 0xC
#define COLOR_YELLOW 0xE
#define COLOR_WHITE 0xF

void term_init(void);
void term_putc(char c);
void term_print(const char *s);
void term_print_ok(const char *s);
void term_print_error(const char *s);
void term_print_panic(const char *s);
void term_print_info(const char *s);
void term_print_debug(const char *s);

#endif
