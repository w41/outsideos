struct segdesc {
	unsigned int seg_limit_15_0 : 16;
	unsigned int seg_base_15_0 : 16;
	unsigned int seg_base_23_16 : 8;
	unsigned int type : 4;
	unsigned int system : 1;
	unsigned int desc_priv_level : 2;
	unsigned int present : 1;
	unsigned int seg_limit_19_16 : 4;
	unsigned int avl : 1;
	unsigned int is64 : 1;
	unsigned int opsize32 : 1;
	unsigned int granularity : 1;
	unsigned int seg_base_31_24 : 8;
} __attribute__((packed));

#define SEG_NULL 0
#define SEG_KCODE 1
#define SEG_KDATA 2
#define SEG_UCODE 3
#define SEG_UDATA 4
#define SEG_TSS 5

#define DEFNULLSEG       \
	(struct segdesc) \
	{                \
		0        \
	}
#define DEFSEG(type, base, lim, dpl)                                           \
	(struct segdesc)                                                       \
	{                                                                      \
		(lim) & 0xffff, (base)&0xffff, ((base) >> 16) & 0xff, type, 1, \
			dpl, 1, ((lim) >> 16) & 0xf, 0, 0, 1, 1,               \
			((base) >> 24) & 0xff                                  \
	}
#define DEFTSS(type, base, lim, dpl)                                           \
	(struct segdesc)                                                       \
	{                                                                      \
		(lim) & 0xffff, (base)&0xffff, ((base) >> 16) & 0xff, type, 0, \
			dpl, 1, ((lim) >> 16) & 0xf, 0, 0, 0, 0,               \
			((base) >> 24) & 0xff                                  \
	}

int gdt_init(void);
void gdt_set_esp0(uint32_t esp0);
