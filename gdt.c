#include <stddef.h>
#include <stdint.h>

#include "console.h"
#include "gdt.h"

static struct segdesc segs[6];

static inline void lgdt(struct segdesc *segs, int size)
{
	volatile uint16_t w[3];
	w[0] = size - 1;
	w[1] = (uintmax_t)segs;
	w[2] = ((uintmax_t)segs) >> 16;

	asm volatile("lgdt (%0)" : : "r"(w));
}

struct tss_entry {
	uint32_t prev;
	uint32_t esp0;
	uint32_t ss0;
	uint32_t esp1;
	uint32_t ss1;
	uint32_t esp2;
	uint32_t ss2;
	uint32_t cr3;
	uint32_t eip;
	uint32_t eflags;
	uint32_t eax;
	uint32_t ecx;
	uint32_t edx;
	uint32_t ebx;
	uint32_t esp;
	uint32_t ebp;
	uint32_t esi;
	uint32_t edi;
	uint32_t es;
	uint32_t cs;
	uint32_t ss;
	uint32_t ds;
	uint32_t fs;
	uint32_t gs;
	uint32_t ldts;
	uint16_t t;
	uint16_t iomap_base;
};

struct tss_entry tss;
void gdt_set_esp0(uint32_t esp0)
{
	tss.esp0 = esp0;
}

int gdt_init(void)
{
	memset(&tss, 0, sizeof(tss));
	tss.ss0 = SEG_KDATA << 3;
	/* set tss.esp0 in scheduler! */

	segs[SEG_NULL] = DEFNULLSEG;
	segs[SEG_KCODE] = DEFSEG(0xA, 0x0, 0xffffffff, 0);
	segs[SEG_KDATA] = DEFSEG(0x2, 0x0, 0xffffffff, 0);
	segs[SEG_UCODE] = DEFSEG(0xA, 0, 0xffffffff, 3);
	segs[SEG_UDATA] = DEFSEG(0x2, 0, 0xffffffff, 3);
	segs[SEG_TSS] = DEFTSS(0x9, (int)&tss, sizeof(tss), 3);

	lgdt(segs, sizeof(segs));

	asm volatile("movw $0x10, %ax");
	asm volatile("movw %ax, %ds");
	asm volatile("movw %ax, %es");
	asm volatile("movw %ax, %fs");
	asm volatile("movw %ax, %gs");

	asm volatile("jmp %0, $1f" : : "i"(SEG_KCODE << 3));
	asm volatile("1:");

	asm volatile("mov %0, %%ax" : : "i"(SEG_TSS << 3));
	asm volatile("ltr %ax");

	term_print_ok("Segmentation setup complete.");

	return 0;
}
