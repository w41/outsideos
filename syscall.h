#ifndef SYSCALL_H
#define SYSCALL_H

#include "idt.h"

void syscall(struct trapframe *tf);

#endif
