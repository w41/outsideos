#include "console.h"
#include "lib.h"
#include "ports.h"
#include "sched.h"

enum ps2_device {
	ps2_device_none = 0,
	ps2_device_std_mouse,
	ps2_device_scroll_mouse,
	ps2_device_5button_mouse,
	ps2_device_mf2_transition_keyboard,
	ps2_device_mf2_keyboard,
};

enum modifier_keys {
	MK_CAPSLK = 256,
	MK_RSHIFT,
	MK_ENTER,
	MK_BACKSPACE,
	MK_F1,
	MK_F2,
	MK_F3,
	MK_F4,
	MK_F5,
	MK_F6,
	MK_F7,
	MK_F8,
	MK_F9,
	MK_F10,
	MK_F11,
	MK_F12,
	MK_TAB,
	MK_LALT,
	MK_LSHIFT,
	MK_LCTRL,
};

static int capslk_on = 0;
static int shift_pressed = 0;

static unsigned int keyboard_map[256] = {
	/* 0x0 */ 0,
	MK_F9,
	0,
	MK_F5,
	MK_F3,
	MK_F1,
	MK_F2,
	MK_F12,
	0,
	MK_F10,
	MK_F8,
	MK_F6,
	MK_F4,
	MK_TAB,
	'`',
	0,
	/* 0x10 */ 0,
	MK_LALT,
	MK_LSHIFT,
	0,
	MK_LCTRL,
	'q',
	'1',
	0,
	0,
	0,
	'z',
	's',
	'a',
	'w',
	'2',
	0,
	/* 0x20 */ 0,
	'c',
	'x',
	'd',
	'e',
	'4',
	'3',
	0,
	0,
	' ',
	'v',
	'f',
	't',
	'r',
	'e',
	0,
	/* 0x30 */ 0,
	'n',
	'b',
	'h',
	'g',
	'y',
	'6',
	0,
	0,
	0,
	'm',
	'j',
	'u',
	'7',
	'8',
	0,
	/* 0x40 */ 0,
	',',
	'k',
	'i',
	'o',
	'0',
	'9',
	0,
	0,
	'.',
	'/',
	'l',
	';',
	'p',
	'-',
	0,
	/* 0x50 */ 0,
	0,
	'\'',
	0,
	'[',
	'=',
	0,
	0,
	MK_CAPSLK,
	MK_RSHIFT,
	MK_ENTER,
	']',
	0,
	'\\',
	0,
	0,
	/* 0x60 */ 0,
	0,
	0,
	0,
	0,
	0,
	MK_BACKSPACE,
	0 /* the rest is unhandled for now */
};

static unsigned int keyboard_map_upper[256] = {
	['`'] = '~',  ['1'] = '!', ['2'] = '@',	 ['3'] = '#', ['4'] = '$',
	['5'] = '%',  ['6'] = '^', ['7'] = '&',	 ['8'] = '*', ['9'] = '(',
	['0'] = ')',  ['-'] = '_', ['='] = '+',	 ['['] = '{', [']'] = '}',
	['\\'] = '|', [';'] = ':', ['\''] = '"', [','] = '<', ['.'] = '>',
	['/'] = '?',
};

static inline void in_wait(void)
{
	while (!(inb(0x64) & 1)) {
	}
}

static inline void out_wait(void)
{
	while (inb(0x64) & 2) {
	}
}

int keyboard_init(void)
{
	uint8_t status;
	int num_devices = 0;
	int working[2];

	/* We should check USB & disable USB Legacy Mode here. */
	/* And also, determine if the controller exists via ACPI. */

	/* Disable all devices. */
	outb(0x64, 0xA0);
	out_wait();
	outb(0x64, 0xA7);

	/* Flush the output buffer. */
	(void)inb(0x60);

	/* Configure the controller. */
	out_wait();
	outb(0x64, 0x20);
	in_wait();
	status = inb(0x60);
	out_wait();
	outb(0x64, 0x60);
	out_wait();
	status &= ~99;
	outb(0x60, status);

	/* If bit 5 of the status byte is zero, we have determined there's
	 * only 1 device. */
	if (!(status & (1 << 5)))
		num_devices = 1;

	/* The 8042 self-check. */
	out_wait();
	outb(0x64, 0xAA);
	in_wait();
	if (inb(0x60) != 0x55)
		goto error;

	/* Checking if there are 2 devices by trying to enable/disable
	 * the second one. */
	if (num_devices == 0) {
		out_wait();
		outb(0x64, 0xA8);
		in_wait();
		status = inb(0x60);
		if (status & (1 << 5)) {
			num_devices = 1;
		} else {
			num_devices = 2;
			out_wait();
			outb(0x64, 0xA7);
		}
	}

	/* Check devices. */
	out_wait();
	outb(0x64, 0xAB);
	in_wait();
	working[0] = inb(0x60) == 0;
	if (num_devices == 2) {
		out_wait();
		outb(0x64, 0xA9);
		in_wait();
		working[1] = inb(0x60) == 0;
	} else {
		working[1] = 0;
	}

	if (!working[0] && !working[1])
		goto error;

	/* Enable devices and interrupts. */
	if (working[0]) {
		out_wait();
		outb(0x64, 0xAE);
		out_wait();
		outb(0x64, 0x60);
		out_wait();
		status |= 1;
		outb(0x60, status);
	}
	if (working[1]) {
		out_wait();
		outb(0x64, 0xA8);
		out_wait();
		outb(0x64, 0x60);
		out_wait();
		status |= 2;
		outb(0x60, status);
	}

	/* Reset devices. */
	if (working[0]) {
		out_wait();
		outb(0x60, 0xFF);

		in_wait();
		if (inb(0x60) != 0xFA)
			goto error;
	}
	if (working[1]) {
		out_wait();
		outb(0x64, 0xD4);
		out_wait();
		outb(0x60, 0xFF);

		in_wait();
		if (inb(0x60) != 0xFA)
			goto error;
	}

	/* Print some info. */
	if (working[0]) {
		term_print_info("PS/2 device #1 available.");
	} else {
		term_print_info("PS/2 device #1 not available.");
	}
	if (working[1]) {
		term_print_info("PS/2 device #2 available.");
	} else {
		term_print_info("PS/2 device #2 not available.");
	}

	term_print_ok("PS/2 controller initialized.");
	return 0;
error:
	term_print_error("PS/2 controller ded :(");
	return 1;
}

void keyboard_getc(void)
{
	static int skip = 0;
	if (skip > 0) {
		skip--;
		return;
	}

	unsigned char c = inb(0x60);
	unsigned int kc;
	int pressed = 1;
	if (c == 0xf0) {
		in_wait();
		c = inb(0x60);
		pressed = 0;
		skip = 1;
	}

	kc = keyboard_map[c];
	if (kc == 0)
		return;
	if (kc < 256 && pressed) {
		/* character key,
		 * let's see if we should output lower or upper case */
		if (!capslk_on && !shift_pressed)
			tty_putchar(current_task->tty, kc);
		else if ('a' <= kc && kc <= 'z')
			tty_putchar(current_task->tty, kc - 'a' + 'A');
		else if (shift_pressed && keyboard_map_upper[kc] != 0)
			tty_putchar(current_task->tty, keyboard_map_upper[kc]);
	} else {
		if (kc == MK_CAPSLK && pressed)
			capslk_on = !capslk_on;
		else if (kc == MK_ENTER && pressed)
			tty_putchar(current_task->tty, '\n');
		else if (kc == MK_LSHIFT || kc == MK_RSHIFT)
			shift_pressed = pressed;
	}
}
