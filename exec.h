#ifndef EXEC_H
#define EXEC_H

int execve(const char *pathname, char *const argv[], char *const envp[]);
int sys_execve(struct trapframe *tf);

#endif
