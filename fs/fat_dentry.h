#ifndef FS__FAT_DENTRY_H
#define FS__FAT_DENTRY_H

#include "fat.h"

#define D_ROOT 0x1
#define D_DIRECTORY 0x2

#define DENTRY_NAME_EMBEDDED_SIZE 20 /* bring the size to 32 */

struct dentry_name {
	enum {
		FAT_NAME,
		UNIX_NAME
	} format; /* change this on name conversion */
	int length; /* total_length */
	int allocated_size; /* 0 if name is embedded,
			       >0 when we have allocated some memory */
	char *rest_of_name;
	char shortname[DENTRY_NAME_EMBEDDED_SIZE];
};

struct dentry {
	struct dentry_name *name;
	struct inode *inode;
	struct dentry *parent;
	struct dentry_children_list {
		struct dentry *dentry;
		struct dentry_children_list *next;
	} * children_list;
	int flags;
	int instantiated;
};

struct inode;

struct dentry *alloc_dentry(struct dentry *parent, int flags,
			    struct dentry_name *name);
void free_dentry(struct dentry *dentry);

struct dentry *path_lookup(const char *path, int flags);

#endif
