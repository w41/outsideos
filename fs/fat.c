#include "fat.h"
#include "fat_dentry.h"

#include "console.h"
#include "file.h"
#include "lib.h"
#include "panic.h"

#include <stdint.h>

struct superblock *root_sb;

int fat_init()
{
	term_print_ok("FAT driver initialized.");
}

#define create_fatdate(dd, mm, yy) (dd | ((mm + 1) << 4) | ((yy - 1980) << 8))

#define get_year_from_fatdate(fd) ((fd >> 8) - 1980)
#define get_month_from_fatdate(fd) (((fd >> 4) & 0xF) - 1)
#define get_day_from_fatdate(fd) (fd & 0xF)

#define create_fattime(s, m, h) ((s >> 1) | (m << 4) | (h << 10))

#define get_seconds_from_fattime(ft) ((ft >> 9) & 0xE)
#define get_minutes_from_fattime(ft) ((ft >> 4) & 0x3F)
#define get_hours_from_fattime(ft) (ft & 0x1F)

#define ATTR_READ_ONLY 0x01
#define ATTR_HIDDEN 0x02
#define ATTR_SYSTEM 0x04
#define ATTR_VOLUME_ID 0x08
#define ATTR_DIRECTORY 0x10
#define ATTR_ARCHIVE 0x20
#define ATTR_LONG_NAME \
	(ATTR_READ_ONLY | ATTR_HIDDEN | ATTR_SYSTEM | ATTR_VOLUME_ID)

#define LAST_LONG_DENTRY 0x40
#define LONG_DENTRY_STRLEN 13

struct superblock {
	struct mem_blkdev *dev;

	int bytes_per_sector;
	int sectors_per_cluster;
	int num_reserved_sectors;
	int num_fats;
	int num_root_entries;
	int num_sectors_16;
	int media;
	int sectors_per_fat_16;
	int sectors_per_track;
	int num_heads;
	int num_hidden_sectors;
	int num_sectors_32;

	union {
		struct {
			int drive_number;
			int boot_signature;
			int volume_id;
			char volume_label[FAT_SHORTNAME_LENGTH];
			char filesys_type[8];
		} fat16;
		struct {
			int sectors_per_fat_32;
			int ext_flags;
			int fs_version;
			int root_first_cluster;
			int fsinfo_sector;
			int backup_br_sector;
			int drive_number;
			int boot_signature;
			int volume_id;
			char volume_label[FAT_SHORTNAME_LENGTH];
		} fat32;
	};

	int root_dir_sector;
	int cluster_size;

	uint32_t next_inode_no;

	/* FIXME: Only supporting fat32... */
	enum fat_type { FAT12, FAT16, FAT32 } fat_type;
};

static uint32_t read_fat_entry(struct superblock *sb, int cluster)
{
	int fat_sector =
		sb->num_reserved_sectors + (cluster * 4) / sb->bytes_per_sector;
	int fat_offset = (cluster * 4) % sb->bytes_per_sector;
	uint32_t fat_entry;
	if (mem_blkdev_read(sb->dev,
			    fat_sector * sb->bytes_per_sector + fat_offset, 4,
			    (char *)&fat_entry) != 4)
		return 0;

	if ((fat_entry & 0x0FFFFFFF) == 0x0FFFFFF7) {
		term_print_error("Encountered a bad FAT cluster.");
		return 0;
	} else if ((fat_entry & 0x0FFFFFFF) == 0) {
		panic("Encountered a free FAT cluster during a normal chain walk.");
	}

	return fat_entry;
}

int read_inode(struct inode *inode, char *buf, int offset, int count)
{
	struct superblock *sb = inode->sb;
	//if (offset + count > sb->cluster_size)
	//      panic("reading multi-cluster file not implemented.");

	int cluster_start_idx = offset / sb->cluster_size;
	int cluster_end_idx = (offset + count) / sb->cluster_size;

	/* Skipping through to cluster_start... */
	int current_cluster_idx = 0;
	uint32_t current_cluster =
		inode->cluster + 2 -
		sb->root_dir_sector / sb->sectors_per_cluster;
	while (current_cluster_idx < cluster_start_idx) {
		/* Read the FAT entry for the cluster. */
		current_cluster = read_fat_entry(sb, current_cluster);
		if (current_cluster < 0xFFFFFF7)
			current_cluster_idx++;
		else
			return 0;
	}

	/* Check for the early loop break -- the file is too short. */
	if (current_cluster_idx != cluster_start_idx)
		return 0;

	int start = offset % sb->cluster_size;
	int end = current_cluster_idx == cluster_end_idx ?
			  ((offset + count) % sb->cluster_size) :
			  sb->cluster_size;
	int retval = 0;
	/* We are ready to start filling buf. */
	do {
		if (mem_blkdev_read(
			    sb->dev,
			    (current_cluster - 2 +
			     sb->root_dir_sector / sb->sectors_per_cluster) *
					    sb->cluster_size +
				    start,
			    end - start, buf) != end - start)
			;
		buf += end - start;
		count -= end - start;
		retval += end - start;

		if (count == 0)
			break;

		current_cluster = read_fat_entry(sb, current_cluster);
		if (current_cluster < 0xFFFFFF7)
			current_cluster_idx++;
		else
			return 0;

		start = 0;
		end = current_cluster_idx == cluster_end_idx ? count :
							       sb->cluster_size;
	} while (current_cluster_idx <= cluster_end_idx);

	return retval;
}

static int write_inode(struct inode *inode, const char *buf, int offset,
		       int count)
{
	panic("write_inode() not implemented.");
}

static struct inode *instantiate_inode(struct superblock *sb, char *buf,
				       int *offset, int flags)
{
	int buf_allocated = 0;
	struct inode *inode = kmalloc(sizeof(struct inode));
	if (!inode)
		return 0;

	inode->flags = flags;
	inode->sb = sb;
	inode->inode_no = sb->next_inode_no++;
	if (!buf) {
		buf = kmalloc(32);
		if (!buf)
			goto error_free_inode;

		buf_allocated = 1;

		if (mem_blkdev_read(sb->dev, *offset, 32, buf) != 32)
			goto error_free_buf;
	}

	/* After handling long dentries, buf contains the underlying short dentry. */
	inode->attr = buf[11];
	inode->ctime = *(uint16_t *)(buf + 14);
	inode->cdate = *(uint16_t *)(buf + 16);
	inode->adate = *(uint16_t *)(buf + 18);
	inode->wtime = *(uint16_t *)(buf + 22);
	inode->wdate = *(uint16_t *)(buf + 24);
	inode->cluster = sb->root_dir_sector / sb->sectors_per_cluster +
			 (*(uint16_t *)(buf + 20) << 16) +
			 *(uint16_t *)(buf + 26) -
			 (inode->flags & I_ROOT ? 0 : 2);
	inode->size = *(uint32_t *)(buf + 28);
	inode->mode = (flags & I_DIRECTORY) ? S_IFDIR : S_IFREG;

	if (buf_allocated)
		kfree(buf);

	return inode;

error_free_buf:
	if (buf_allocated)
		kfree(buf);
error_free_inode:
	kfree(inode);
	return 0;
}

#define long_entry_read_char(b)                                         \
	do {                                                            \
		if ((b)[1] != 0)                                        \
			term_print_error(                               \
				"Multibyte characters not supported."); \
		retval.str[retval.length++] = (b)[0];                   \
		if (!(b)[0])                                            \
			return retval;                                  \
	} while (0)

struct long_entry_str {
	char str[LONG_DENTRY_STRLEN];
	int length;
} read_long_entry_str(char *buf)
{
	struct long_entry_str retval;
	long_entry_read_char(buf + 1);
	long_entry_read_char(buf + 3);
	long_entry_read_char(buf + 5);
	long_entry_read_char(buf + 7);
	long_entry_read_char(buf + 9);
	long_entry_read_char(buf + 14);
	long_entry_read_char(buf + 16);
	long_entry_read_char(buf + 18);
	long_entry_read_char(buf + 20);
	long_entry_read_char(buf + 22);
	long_entry_read_char(buf + 24);
	long_entry_read_char(buf + 28);
	long_entry_read_char(buf + 30);

	return retval;
}

int instantiate_directory(struct superblock *sb, struct dentry *parent)
{
	struct inode *inode = parent->inode;
	int offset = inode->flags & I_ROOT ? 32 : 0;
	char buf[32];
	struct dentry_name *name;

	if (parent->instantiated)
		return 0;

	while (1) {
		if (read_inode(inode, buf, offset, 32) != 32)
			return 1;

		if (buf[0] == 0) {
			offset += 32;
			break;
		}

		if (buf[0] == 0xE5) {
			offset += 32;
			continue;
		}

		if (buf[11] == ATTR_LONG_NAME) {
			if (!(buf[0] & LAST_LONG_DENTRY)) {
				term_print_error(
					"Long dentry without a LAST_LONG_DENTRY tag found.");
				return 1;
			}

			/* Skip to the first long dentry and
			 * move backwards from there.
			 * */
			int num_long_entries = buf[0] - LAST_LONG_DENTRY;
			name = kmalloc(sizeof(struct dentry_name));

			/* Calculating the length... */
			int length = 0;
			for (int i = num_long_entries - 1; i >= 0; i--) {
				if (read_inode(inode, buf, offset + i * 32,
					       32) != 32)
					goto error_free_dentry_name;

				struct long_entry_str str =
					read_long_entry_str(buf);
				length += str.length;
			}

			name->length = length;

			if (length >= DENTRY_NAME_EMBEDDED_SIZE) {
				name->allocated_size =
					length - DENTRY_NAME_EMBEDDED_SIZE;
				name->rest_of_name =
					kmalloc(name->allocated_size);
			}

			int rest_of_name_offset = 0;
			int shortname_offset = 0;
			/* FIXME: This should require only one pass... */
			for (int i = num_long_entries - 1; i >= 0; i--) {
				if (read_inode(inode, buf, offset + i * 32,
					       32) != 32)
					goto error_free_dentry_name;
				struct long_entry_str str =
					read_long_entry_str(buf);
				int str_offset = 0;

				if (rest_of_name_offset <
				    name->allocated_size) {
					int to_read = min(
						LONG_DENTRY_STRLEN,
						length -
							DENTRY_NAME_EMBEDDED_SIZE);

					strncpy(name->rest_of_name +
							rest_of_name_offset,
						str.str, to_read);
					rest_of_name_offset += to_read;
					str_offset += to_read;
				}

				if (rest_of_name_offset ==
				    name->allocated_size) {
					int to_read = str.length - str_offset;
					strncpy(name->shortname +
							shortname_offset,
						str.str + str_offset, to_read);
					shortname_offset += to_read;
				}
			}

			offset += 32 * num_long_entries;
			continue;
		} else if (buf[0] == '.' && buf[1] == ' ' ||
			   buf[0] == '.' && buf[1] == '.' && buf[2] == ' ') {
			/* FIXME: Skip dot and dot-dot entries and simulate them later if needed. */
			offset += 32;
			continue;
		}

		/* We have catered for all side cases, if we're here then buf
		 * contains a genuine dentry.
		 * */
		struct dentry *dentry = alloc_dentry(
			parent, buf[11] & ATTR_DIRECTORY ? D_DIRECTORY : 0,
			name);
		struct inode *inode =
			instantiate_inode(sb, buf, &offset, I_DIRECTORY);
		dentry->inode = inode;

		offset += 32;
	}

	parent->instantiated = 1;
	return 0;

error_free_dentry_name:
	kfree(name);
	return 1;
}

static int instantiate_root(struct superblock *sb)
{
	int offset = sb->root_dir_sector * sb->bytes_per_sector;
	struct inode *root_inode =
		instantiate_inode(sb, 0, &offset, I_ROOT | I_DIRECTORY);
	struct dentry *root_dentry = alloc_dentry(0, D_ROOT | D_DIRECTORY, 0);
	root_dentry->inode = root_inode;
	instantiate_directory(sb, root_dentry);

	root_sb = sb;
	return 0;
}

struct superblock *create_from_device(struct mem_blkdev *dev)
{
	unsigned char preheader[36];
	struct superblock *sb;
	if (mem_blkdev_read(dev, 0, 36, preheader) != 36)
		return 0;

	if (preheader[0] != 0xE9 && preheader[0] != 0xEB)
		return 0;

	sb = kmalloc(sizeof(struct superblock));
	if (!sb)
		return 0;

	sb->dev = dev;
	sb->bytes_per_sector = *(uint16_t *)(preheader + 11);
	sb->sectors_per_cluster = preheader[13];
	sb->num_reserved_sectors = *(uint16_t *)(preheader + 14);
	sb->num_fats = preheader[16];
	sb->num_root_entries = *(uint16_t *)(preheader + 17);
	sb->num_sectors_16 = *(uint16_t *)(preheader + 19);
	sb->media = preheader[21];
	sb->sectors_per_fat_16 = *(uint16_t *)(preheader + 22);
	sb->sectors_per_track = *(uint16_t *)(preheader + 24);
	sb->num_heads = *(uint16_t *)(preheader + 26);
	sb->num_hidden_sectors = *(uint32_t *)(preheader + 28);
	sb->num_sectors_32 = *(uint32_t *)(preheader + 32);

	if (sb->sectors_per_fat_16 != 0) {
		term_print_error(
			"FAT32 is the only supported FAT format, fs not created.");
		goto error_free_sb;
	}

	/* Only FAT32 past this point. */

	sb->fat_type = FAT32;
	unsigned char fat32_header[54];
	if (mem_blkdev_read(dev, 36, 54, fat32_header) != 54)
		goto error_free_sb;

	sb->fat32.sectors_per_fat_32 = *(uint32_t *)fat32_header;
	sb->fat32.ext_flags = *(uint16_t *)(fat32_header + 4);
	sb->fat32.fs_version = *(uint16_t *)(fat32_header + 6);
	sb->fat32.root_first_cluster = *(uint32_t *)(fat32_header + 8);
	sb->fat32.fsinfo_sector = *(uint16_t *)(fat32_header + 12);
	sb->fat32.backup_br_sector = *(uint16_t *)(fat32_header + 14);
	sb->fat32.drive_number = *(uint8_t *)(fat32_header + 28);
	sb->fat32.boot_signature = *(uint8_t *)(fat32_header + 30);
	sb->fat32.volume_id = (fat32_header[34] << 24) +
			      (fat32_header[33] << 16) +
			      (fat32_header[32] << 8) + fat32_header[31];
	memcpy(sb->fat32.volume_label, fat32_header + 35, FAT_SHORTNAME_LENGTH);

	sb->root_dir_sector = sb->num_reserved_sectors +
			      sb->num_fats * sb->fat32.sectors_per_fat_32;
	sb->cluster_size = sb->bytes_per_sector * sb->sectors_per_cluster;
	sb->next_inode_no = 0;
	instantiate_root(sb);

#ifdef FS_DEBUG
	char str[150] = "";
	sprintf(str,
		"Created new superblock from dev 0x%x: BPS %d, SPC %d, #RSVD %d, "
		"#FAT %d, totsec16 = %d, totsec32 = %d, rootdir @sector %d, label %s",
		dev, sb->bytes_per_sector, sb->sectors_per_cluster,
		sb->num_reserved_sectors, sb->num_fats, sb->num_sectors_16,
		sb->num_sectors_32,
		sb->num_reserved_sectors +
			(sb->num_fats * sb->fat32.sectors_per_fat_32),
		sb->fat32.volume_label);

	term_print_debug(str);

	memset(str, 0, 150);
	sprintf(str, "path_lookup_results: 0x%x 0x%x 0x%x",
		path_lookup("/out/in/testfile", 0), path_lookup("/prog.c", 0),
		path_lookup("/bin/prog", 0));
	term_print_debug(str);
#endif

	return sb;

error_free_sb:
	kfree(sb);
	return 0;
};
