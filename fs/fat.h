#ifndef FS__FAT_H
#define FS__FAT_H

#include "../dev/mem_blkdev.h"

#include <stdint.h>

#define FAT_FILENAME_LENGTH 8
#define FAT_EXTENSION_LENGTH 3
#define FAT_SHORTNAME_LENGTH (FAT_FILENAME_LENGTH + FAT_EXTENSION_LENGTH)

struct superblock;
struct dentry;

/* Inode flags */
#define I_ROOT 0x1
#define I_DIRECTORY 0x2

struct inode {
	uint8_t attr;
	uint16_t ctime;
	uint16_t cdate;
	uint16_t adate;
	uint16_t wtime;
	uint16_t wdate;

	int flags;
	int mode;

	/* This cluster number is always maintained equal to (device offset / cluster_size),
	 * and not using the horrible FAT numbering scheme.
	 * */
	uint32_t cluster;
	uint32_t size;

	uint32_t inode_no;

	struct superblock *sb;
};

int fat_init(void);
struct superblock *create_from_device(struct mem_blkdev *dev);
int instantiate_directory(struct superblock *sb, struct dentry *parent);

int read_inode(struct inode *inode, char *buf, int offset, int count);

extern struct superblock *root_sb;
#endif
