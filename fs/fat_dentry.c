#include "fat_dentry.h"
#include "fat.h"

#include "lib.h"
#include "panic.h"
#include "sched.h"

#define DCACHE_SIZE 256

#define DENTRY_DIRECTORY 0x1
#define DENTRY_ROOT 0x2

struct dentry *root;

struct dentry_cache {
	const char *paths[DCACHE_SIZE];
	struct dentry *dentries[DCACHE_SIZE];
} * dentry_cache;

static void convert_fat_name_to_regular(char *fatname, char *regular)
{
	for (int i = 0; i < FAT_FILENAME_LENGTH; i++) {
		if (fatname[i] == ' ')
			break;

		*regular = fatname[i];

		if (likely('A' <= *regular && *regular <= 'Z'))
			*regular -= 'A' - 'a';

		regular++;
	}

	for (int i = FAT_FILENAME_LENGTH; i < FAT_SHORTNAME_LENGTH; i++) {
		if (fatname[i] == ' ')
			break;

		if (i == FAT_FILENAME_LENGTH)
			*regular++ = '.';

		*regular = fatname[i];
		if (('A' <= *regular && *regular <= 'Z'))
			*regular -= 'A' - 'a';

		regular++;
	}
}

static int filename_letters_equal(char c1, char c2)
{
	if ('A' <= c1 && c1 <= 'Z')
		return (c2 == c1 || c2 == c1 - 'A' + 'a');

	if ('a' <= c1 && c1 <= 'z')
		return (c2 == c1 || c2 == c1 - 'a' + 'A');

	return c1 == c2;
}

static int filename_parts_equal(const char *str1, const char *str2, int len)
{
	for (int i = 0; i < len; i++) {
		if (str1[i] == ' ')
			if (str2[i] == ' ')
				break;
			else
				return 0;

		if (!filename_letters_equal(str1[i], str2[i]))
			return 0;
	}

	return 1;
}

static int filenames_equal_maxlen(const char *str1, const char *str2,
				  int maxlen)
{
	if (!filename_parts_equal(str1, str2, min(maxlen, FAT_FILENAME_LENGTH)))
		return 0;

	if (maxlen > FAT_FILENAME_LENGTH)
		return filename_parts_equal(str1 + FAT_FILENAME_LENGTH,
					    str2 + FAT_FILENAME_LENGTH,
					    FAT_EXTENSION_LENGTH);
}

static int filenames_equal(const char *str1, const char *str2)
{
	return filenames_equal_maxlen(str1, str2, FAT_FILENAME_LENGTH);
}

static int dentry_name_equals_maxlen(struct dentry *dentry, const char *str,
				     int maxlen)
{
	if (dentry->name->allocated_size > 0)
		return !strncmp(dentry->name->rest_of_name, str,
				dentry->name->allocated_size) &&
			       (dentry->name->allocated_size >= maxlen) ||
		       !strncmp(dentry->name->shortname,
				str + dentry->name->allocated_size,
				maxlen - dentry->name->allocated_size);

	else
		return !strncmp(dentry->name->shortname, str, maxlen);
}

static struct dentry *find_child_dentry(struct superblock *sb,
					struct dentry *parent, const char *name,
					int namesize)
{
	/* FIXME: Look in the dentry cache first... */

	if (namesize == 1 && name[0] == '.')
		return parent;

	if (namesize == 2 && name[0] == '.' && name[1] == '.')
		return parent->parent ? parent->parent : parent;

	for (struct dentry_children_list *child = parent->children_list;
	     child != 0; child = child->next) {
		if (dentry_name_equals_maxlen(child->dentry, name, namesize)) {
			instantiate_directory(sb, child->dentry);
			return child->dentry;
		}
	}
}

struct dentry *path_lookup(const char *path, int flags)
{
	struct dentry *current_dir;
	struct superblock *sb = 0;
	if (path[0] == '/') {
		/* the path is absolute */
		current_dir = root;
		sb = root_sb;
		path++;
	} else {
		current_dir = current_task->cwd;
		sb = root_sb;
	}

	const char *start = path, *end;
	while ((end = strchr(start, '/')) != 0) {
		current_dir =
			find_child_dentry(sb, current_dir, start, end - start);
		if (!current_dir)
			return 0;
		term_print_info(start);
		start = end + 1;
	}

	/* FIXME: If *start is 0 here, that means we must have set O_DIRECTORY.
	 * Otherwise, it points to a file name.
	 */
	if (!*start)
		return (current_dir->flags & D_DIRECTORY) ? current_dir : 0;
	return find_child_dentry(sb, current_dir, start, strlen(start));
}

struct dentry *alloc_dentry(struct dentry *parent, int flags,
			    struct dentry_name *name)
{
	struct dentry *dentry = kmalloc(sizeof(struct dentry));
	if (!dentry)
		return 0;

	dentry->name = name;
	dentry->parent = parent;
	dentry->flags = flags;
	dentry->instantiated = 0;
	dentry->children_list = kmalloc(sizeof(struct dentry_children_list));
	if (!dentry->children_list)
		goto error_free_dentry;

	if (dentry->flags & D_ROOT) {
		root = dentry;
	} else {
		/* insert dentry into parent's children_list... */
		if (!parent->children_list->dentry) {
			parent->children_list->dentry = dentry;
		} else {
			struct dentry_children_list *node;
			for (node = parent->children_list; node->next;
			     node = node->next) {
			}
			node->next =
				kmalloc(sizeof(struct dentry_children_list));
			if (!node->next)
				goto error_free_children_list;
			node->next->dentry = dentry;
			node->next->next = 0;
		}
	}

	return dentry;

error_free_children_list:
	kfree(dentry->children_list);
error_free_dentry:
	kfree(dentry);
	return 0;
}

void free_dentry(struct dentry *dentry)
{
	/* First, let's delete ourselves from parent's children_list. */
	struct dentry *parent = dentry->parent;
	for (struct dentry_children_list *node = parent->children_list,
					 *prev = 0;
	     node->dentry != dentry && node->next;
	     prev = node, node = node->next) {
		if (!node->next)
			panic("dentry not found in parent's children_list");

		if (!prev)
			parent->children_list = parent->children_list->next;
		else
			prev->next = node->next;

		kfree(node);
	}

	if (dentry->name->allocated_size > 0)
		kfree(dentry->name->rest_of_name);
	kfree(dentry->name);

	/* FIXME: Currently, freeing dentry's children should be done manually.
	 * Therefore, deleting a non-leaf dentry will FUCK THINGS UP.
	 * */

	kfree(dentry);
}
