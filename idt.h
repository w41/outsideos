#ifndef IDT_H
#define IDT_H

#include <stdint.h>

struct idtdesc {
	unsigned int idt_offset_15_0 : 16;
	unsigned int segment_selector : 16;
	unsigned int zero1 : 8;
	unsigned int type : 4;
	//	unsigned int size : 1;
	unsigned int zero2 : 1;
	unsigned int dpl : 2;
	unsigned int present : 1;
	unsigned int idt_offset_31_16 : 16;
} __attribute__((packed));

#define DEFINT(offset, seg, dpl)                           \
	(struct idtdesc)                                   \
	{                                                  \
		(offset) & 0xffff, seg, 0, 0xE, 0, dpl, 1, \
			((offset >> 16) & 0xffff)          \
	}

#define DEFTRAP(offset, seg, dpl)                          \
	(struct idtdesc)                                   \
	{                                                  \
		(offset) & 0xffff, seg, 0, 0xF, 0, dpl, 1, \
			((offset >> 16) & 0xffff)          \
	}

#define DEFNP                \
	(struct idtdesc)     \
	{                    \
		.present = 0 \
	}

//PAGEBREAK: 36
// Layout of the trap frame built on the stack by the
// hardware and by trapasm.S, and passed to trap().
struct trapframe {
	// registers as pushed by pusha
	unsigned int edi;
	unsigned int esi;
	unsigned int ebp;
	unsigned int oesp; // useless & ignored
	unsigned int ebx;
	unsigned int edx;
	unsigned int ecx;
	unsigned int eax;

	// rest of trap frame
	unsigned short gs;
	unsigned short padding1;
	unsigned short fs;
	unsigned short padding2;
	unsigned short es;
	unsigned short padding3;
	unsigned short ds;
	unsigned short padding4;
	unsigned int trapno;

	// below here defined by x86 hardware
	unsigned int err;
	unsigned int eip;
	unsigned short cs;
	unsigned short padding5;
	unsigned int eflags;

	// below here only when crossing rings, such as from user to kernel
	unsigned int esp;
	unsigned short ss;
	unsigned short padding6;
};

extern uint64_t jiffies;

int idt_init(void);
void unmask_pic(void);

#endif
