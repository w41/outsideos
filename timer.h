#ifndef TIMER_H
#define TIMER_H

#include "ports.h"

#define PIT_CH0 0x40
#define PIT_CMD 0x43

#define HZ 100
#define PIT_HZ 1193180

void timer_init(void);

#endif
