#include "panic.h"

#include "console.h"

void panic(const char *msg)
{
	term_print_panic(msg);
	asm volatile("cli");
	asm volatile("hlt");
}
